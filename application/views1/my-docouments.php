<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xl-12 col-12">
          <div class="box bg-primary-light">
            <div class="box-body d-flex px-0">
              <div class="flex-grow-1 p-30 flex-grow-1 bg-img dask-bg bg-none-md" style="background-position: right bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-1.svg)">
                <div class="row ">
                  <div class="col-12 col-xl-7">
                    <h2>Hii!..., <strong><?php echo $this->session->userdata('name'); ?>!</strong></h2>
                  </div>
                  <div class="col-12 col-xl-5"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-4">
              
                <div class="box">
                  <div class="box-body d-flex p-0">
                    <div class="flex-grow-1 bg-danger p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
                      <h4 class="font-weight-400">Admit Card</h4>
                      <p class="font-size-20">
                        <?php if($admitcards){?>
                        <a href="<?=base_url('admit-card/'.base64_encode($this->session->userdata('id')))?>" target="_blank">Download Admit Card <i class="fa fa-download"></i></a>
                        <?php }else{?>
                      <a href="javascript:void(0)" >No Admit Card Upload</a>
                      <?php }?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </a>
            <div class="col-xl-4">
            
              <div class="box">
                <div class="box-body d-flex p-0">
                  <div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
                    <h4 class="font-weight-400">Mark Sheet</h4>
                    <p class="font-size-20">
                    <?php if($admitcards){?>
                        <a href="<?=base_url('mark-sheet/'.base64_encode($this->session->userdata('id')))?>" class="text-success" target="_blank">Download Mark Sheet <i class="fa fa-download"></i></a>
                        <?php }else{?>
                      <a href="javascript:void(0)" class="text-danger">No Marksheet Upload</a>
                      <?php }?>
                    </p>
                  </div>
                </div>
              </div>
            
            </div>
            <div class="col-xl-4">
           
              <div class="box">
                <div class="box-body d-flex p-0">
                  <div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
                    <h4 class="font-weight-400">Certificats</h4>
                    <p class="font-size-20">
                    <?php if($user->certificate_status==1){?>
                        <a href="<?=base_url('certificate/'.base64_encode($this->session->userdata('id')))?>" class="text-success" target="_blank">Download Certificate <i class="fa fa-download"></i></a>
                        <?php }else{?>
                      <a href="javascript:void(0)" class="text-danger">No Certificate Upload</a>
                      <?php }?>
                    </p>
                  </div>
                </div>
              </div>
           
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
</div>
<!-- /.content-wrapper -->