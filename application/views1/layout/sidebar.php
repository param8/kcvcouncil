<aside class="main-sidebar">
  <!-- sidebar-->
  <section class="sidebar position-relative">
    <div class="multinav">
      <div class="multinav-scroll" style="height: 100%; overflow:auto">
        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">Dashboard</li>
          <li class="">
            <a href="<?=base_url('dashboard')?>">
              <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
              <span>Dashboard</span>
          </li>
          <li class="header">Components </li>
          <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-graduation-cap"><span class="path1"></span><span class="path2"></span></i>
                <span>Courses/subject</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
            <ul class="treeview-menu">
              <li><a href="<?//=base_url('courses-list')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Courses</a></li>
              <li><a href="<?//=base_url('subjects')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Subject</a></li>
            </ul>
          </li> -->

   <?php if($this->session->userdata('user_type')=='Admin'){?>
          <li class="treeview">
              <a href="#">
                <i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
                <span>Users</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
            <ul class="treeview-menu">
              <li><a href="<?=base_url('students')?>"><i class="fa fa-user"><span class="path1"></span><span class="path2"></span></i>Students</a></li>
              <li><a href="<?=base_url('agents')?>"><i class="fa fa-user-secret"><span class="path1"></span><span class="path2"></span></i>Agent</a></li>
              <li><a href="<?=base_url('vendors')?>"><i class="fa fa-address-card"><span class="path1"></span><span class="path2"></span></i>Vendor</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
            <i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
            <span>Settings</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?=base_url('about')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>About US</a></li>
              <li> <a href="<?=base_url('enquiry-list')?>"><i class="fa fa-info-circle"><span class="path1"></span><span class="path2"></span></i><span>Enquiry List</span>
              <li><a href="<?=base_url('site-info')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>Site Setting</a></li>  
            </ul>
          </li>  
          <?php } if($this->session->userdata('user_type')=='Agent'){?>		
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
                <span>Users</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
            <ul class="treeview-menu">
              <li><a href="<?=base_url('students')?>"><i class="fa fa-user"><span class="path1"></span><span class="path2"></span></i>Students</a></li>
             
            </ul>
          </li>
            <?php } if($this->session->userdata('user_type')=='Vendor'){?>		
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
                <span>Users</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
            <ul class="treeview-menu">
              <li><a href="<?=base_url('students')?>"><i class="fa fa-user"><span class="path1"></span><span class="path2"></span></i>Students</a></li>
              
            </ul>
          </li>
            <?php } if($this->session->userdata('user_type')=='Student'){?>		

              <li><a href="<?=base_url('my-docouments')?>"><i class="fa fa-file"><span class="path1"></span><span class="path2"></span></i>My Documents</a></li>
     
            <?php } ?>
        </ul>
      </div>
    </div>
  </section>
  <div class="sidebar-footer">
    <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><span class="icon-Settings-2"></span></a>
    <a href="mailbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><span class="icon-Mail"></span></a>
    <a href="<?=base_url('authantication/adminLogout')?>" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
  </div>
</aside>