<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-user"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="">
        <form action="<?=base_url('user/store')?>" id="addusers" method="POST" enctype="multipart/form-data">
          <div class="row">
          <?php if($user_type == 'Student'){?>
           <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Vendor:</label>
                <select class="form-control" name="vendorID" id="vendorID">
                  <option value="">Select vendor</option>
                  <?php foreach($vendors as $vendor){?>
                  <option value="<?=$vendor->id?>"><?=$vendor->name?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
         <?php } ?>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"><?=$user_type?> Name:</label>
                <input type="text" class="form-control" name="name" id="name">
              </div>
            </div>
        
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Email:</label>
                <input type="text" class="form-control" name="email" id="email">
              </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Contact No:</label>
                <input type="text" class="form-control" name="contact" id="contact" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Address:</label>
                <textarea  class="form-control" name="address" id="address"></textarea>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">State:</label>
                <select class="form-control" name="state" id="state" onchange="getCity(this.value)">
                  <option value="">Select State</option>
                  <?php foreach($states as $state){?>
                  <option value="<?=$state->id?>"><?=$state->name?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">City:</label>
                <select class="form-control" name="city" id="city">
                  <option value="">Select City</option>
                </select>
              </div>
            </div>

            <?php if($user_type == 'Student'){?>
              <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Father Name:</label>
                <input type="text"  class="form-control"   name="father_name" id="father_name" >
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Mother Name:</label>
                <input type="text"  class="form-control"   name="mother_name" id="mother_name" >
              </div>
            </div>
          

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Roll No.:</label>
                <input type="text"  class="form-control"   name="roll_no" id="roll_no" >
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Date of Birth.:</label>
                <input type="date"  class="form-control"   name="dob" id="dob" >
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course.:</label>
                <input type="text"  class="form-control"   name="course" id="course" >
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Start Course:</label>
                <input type="date"  class="form-control"   name="start_date" id="start_date" >
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">End Course:</label>
                <input type="date"  class="form-control"   name="end_date" id="end_date" >
              </div>
            </div>

            <?php } ?>

      
            
          <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
  <!-- /.box -->          
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $("form#addusers").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     formData.append("user_type", '<?= $user_type?>');
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
           var url = "<?=$user_type == 'Student' ? base_url('students') : ($user_type == 'Agent' ? base_url('agents') : base_url('vendors'))?>";
  				setTimeout(function(){
                      window.location=url;
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   function endDateValidate(start_date){
      $('#endDate_div').html(' <div class="form-group"><label for="name" class="col-form-label">End Date:</label><input type="date" class="form-control" min="'+start_date+'" name="end_date" id="end_date" placeholder="End Date"></div>');
    }
  
    $(document).ready(function() {  
  
  $(".add-more").click(function(){  
      var html = $(".copy").html();  
      $(".after-add-more").after(html);  
  });  
  
  $("body").on("click",".remove",function(){   
      $(this).parents(".control-group").remove();  
  });  
  
  }); 
</script>