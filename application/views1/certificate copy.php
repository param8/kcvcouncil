<?php 
  ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=$page_title?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  </head>
  <body style="color:#fdfdfd; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
    <div class="container">
    <?php if($user){
      $markSheet = json_decode($user->mark_sheet);
      // print_r($markSheet);die;
       $marks =array();
       $total_marks = 0;
        $countSubject = count($markSheet);
       
        foreach ($markSheet as $key => $values){
        // print_r($values);
        $totalValuev=0;
          foreach($values as $key1=>$value){
            $marks[] = $key1;
            $vals =  explode(',',$value);
            foreach($vals as $val){
              $totalValuev += $val;
            }
            $total_marks += $totalValuev;
          }
      
        }
      
        $percentage =($total_marks)/$countSubject;
      ?>
      <div style="width:70%;background:white;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);
        border-radius: 20px;">
  
        <center>
          <table width="750px" border="0" cellspacing="0" cellpadding="0" style="background:#fdfdfd;">
            <tbody>
              <tr>
                <td colspan="5"><img src="<?=base_url('public/pdf_img/top-main.jpg')?>" width="750" height="70"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/top-left.jpg')?>" width="61" height="248"></td>
                <td colspan="3" align="center" style="color:black;font-size:15px;">
                  <!-- <img src="logo.svg" width="130" style="margin-bottom: 10px;"><br>This is to acknowledge that -->
                  <table style="width:100%">
                    <tbody>
                      <tr>
                        <td><img src="<?=base_url($siteinfo->site_logo)?>" width="130" style="margin-bottom: 10px;"></td>
                        <td style="padding-left: 10px;"><span style="   line-height: 45px; font-size:  22px;font-weight: bold;/* font-size: 72px; */background: -webkit-linear-gradient(#22859a, #870000);-webkit-background-clip: text;-webkit-text-fill-color: transparent;"><?=$siteinfo->site_name?></span><br>
                          <small style="padding: 8px 1px 10px 0px; font-size: 100%;text-align: center; display: block;width: 70%;margin: auto;font-style: italic;font-weight: bold;color: #22848d;">A Unit of Ramkumar Vidya Shiksha Sanshthan (RVSS) Society Act-21, 1860 </small>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-size: 15px;padding: 6px 0;font-weight: bold;text-align: center;color: #0b5c1f;">A Unit of Ramkumar Vidya Shiksha Sanshthan (RVSS) Society Act-21, 1850</td>
                      </tr>
                      <tr>
                        <td colspan="2" align="center" style="padding: 6px 0;"><span style="    background-color: #162543;color: #fff;font-weight: 500;padding: 3px 9px;display: inline-block;border-radius: 30px;">(50:BRIT QUALIS CERTIFICATION(UK)</span></td>
                      </tr>
                      <tr>
                        <td colspan="2" align="center" style="padding: 6px 0;"><span style="font-size: 15px;font-weight: bold;text-align: center;color: #0a2294;">Govt. of India Regd. No.: UP43D0019377 | Reg. U.P. Govt. No.: KAP/03397/2019-2020</span></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/top-right.jpg')?>" width="66" height="248"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/left.jpg')?>" width="61" height="295"></td>
                <td align="center" colspan="3">
                  <div class="" style="display: flex;     gap: 12px;" >
                    <div class="left" style="width: 8%;">
                      <img src="<?=base_url('public/pdf_img/iso.jpg')?>" alt="" style="width: 100%;    margin-bottom: 15px;">
                      <img src="meme.PNG" alt="" style="width: 100%;    margin-bottom: 15px;">
                      <img src="iso1.PNG" alt="" style="width: 100%;    margin-bottom: 15px;">
                    </div>
                    <div style="    color: #000; width: 92%;     gap: 12px;    display: flex;">
                      <div style="    width: 100%;"  >
                        <h2 style="    font-size: 42px;  color: red;   font-weight: bold; ">Certificate</h2>
                        <p style="    font-size: 20px;     font-weight: 500;     border-bottom: 2px dotted; ">This Certificate Is Proudly Presented To <br> <span style="font-weight: 800;    line-height: 2;"><?=$user->student_name?></span></p>
                        <p style="    font-size: 16px;  font-weight: 700;    line-height: 2;padding-bottom: 5px;
                          border-bottom: 2px dotted;">ATC: RAMKUMAR VIDYA SHIKSHA SANSHTHAN</p>
                      </div>
                      <img src="<?=base_url('public/pdf_img/st.jpg')?>" alt="st" style="width: 23%;height: 150px;">
                    </div>
                  </div>
                  <div class="" style="display: flex;     gap: 12px;" >
                    <div class="left" style="width: 8%;">
                      <img src="<?=base_url('public/pdf_img/mur.PNG')?>" alt="" style="width: 100%;    margin-bottom: 15px;">
                    </div>
                    <div style="    color: #000; width: 92%;     gap: 12px;     ">
                      <div style=" font-weight: 500;     border-bottom: 2px dotted;padding-bottom: 5px;">has passed the prescribed examination with <?=$percentage>94 ? '+A' : ($percentage>84 ? 'A' : ($percentage>69 ? 'B' : 'C')) ?> Grade <?=$percentage?> % Marks has been awarded the 
                        <br><span style="font-weight: bold;  font-size: 17px; padding-bottom: 5px;margin: 0;">DIPLOMA IN COMPUTER APPLICATION (DCA)</span>
                      </div>
                    </div>
                  </div>
      </div>
      <p style="color: #000; font-weight: bold;">(Course Duration: 6 month)</p>
      </td>                  
      <td align="right"><img src="<?=base_url('public/pdf_img/right.jpg')?>" width="66" height="295"></td>
      </tr> 
      <tr>
      <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="115"> </td>
      <td colspan="3">
      <table style="width:100%">
      <tbody>
      <tr>
      <td>
      <div style="display: flex;    align-items: center;    gap: 10px;">
      <img src="<?=base_url('public/pdf_img/scan.png')?>" alt="" style="width: 13%;">
      <div style="display: flex;">
      <div>
      <span style="color: #000;  font-weight: 700;font-size: 18px;">Certificate No.</span> <input type="text" style="width: 49%;border-bottom: 3px dotted #2c2222!important;border: none;" value=".............">
      </div>
      <div>
      <span style="color: #000;  font-weight: 700;font-size: 18px;">Certificate No.</span> <input type="text" style=" width: 49%;border-bottom: 3px dotted #2c2222!important;   border: none;" value=".............">
      </div>
      </div>
      </div>
      </td>
      </tr>
      </tbody>
      </table>
      </td>                   
      <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="115"> </td>
      </tr>
      <tr>
      <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="100"> </td>                   
      <td align="center" valign="bottom">
      <p style="color:#2E2E2E;font-size:17px;text-align:center;margin-bottom:0;font-weight: 700;font-weight: 700;    border-bottom: 2px solid #000;">ADHAR CARD No. 1234 3253 1234</p>
      </td>
      <td align="center" width="100"></td>
      <td align="center" valign="bottom">
      <p style="text-align:center; margin-bottom:0; "><img src="<?=base_url('public/pdf_img/signature.jpg')?>" style="width:135px;" ></p>
      <p style="color:#2E2E2E;font-size: 14px;text-align: center;margin-bottom: 0;font-weight: 500;">
      Ravindran A/I Raghunathan
      <span style="color:#2E2E2E;font-size: 13px;display: block;">Vice President, HR</span></p>
      </td>                             
      <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="100"> </td>
      </tr> 
      <tr>
      <td colspan="5"><img src="<?=base_url('public/pdf_img/bottom.jpg')?>" width="750" height="83"> </td>
      </tr>
      </tbody>
      </table>
      </center>
      <?php } else{?>
        <h1 style="text-align: center; color: red; margin-top:30%"> No Certificate Found</h1>
        <?php } ?>
    </div>
    </div>
  </body>
</html>