
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title=='Agent' ? '<i class="fa fa-user-secret"></i>' : '<i class="fa fa-user"></i>'?> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
		  <div class="col-md-4 col-lg-4">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-lg-8 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="<?=base_url('create-user/'.base64_encode($page_title))?>" class="btn btn-primary btn-sm float-right" >Add <?=$page_title?> <i class="fa fa-plus"></i></a>
            <span class="ml-2">
            <a href="javascript:void(0)" class="btn btn-success btn-sm " data-toggle="modal" data-target="#bulkUploadModal" data-whatever="@mdo">Bulk <?=$page_title?> <i class="fa fa-upload"></i></a>
            </span>
            <span class="ml-2">
            <a href="javascript:void(0)" class="btn btn-success btn-sm " data-toggle="modal" data-target="#markSheetUploadModal" data-whatever="@mdo">Upload Mark Sheet  <i class="fa fa-upload"></i></a>
            </span>

            <span class="ml-2">
            <a href="javascript:void(0)" class="btn btn-success btn-sm " data-toggle="modal" data-target="#admitCardUploadModal" data-whatever="@mdo">Upload Admit Card  <i class="fa fa-upload"></i></a>
            </span>
            </div>
          </div>
				</div>
			  <div class="box">
				<div class="box-header with-border">
			
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
								<th>Action</th>
								<th>Image</th>
								<th>Registration No.</th>
								<th>Roll No.</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Address</th>
								<th>Created Date</th>
								<th>Status</th>
						
							</tr>
						</thead>
						<tbody>
              <?php 
               $markSheetStudentID =array();
               $admitCardStudentID =array();
               foreach($marksheets as $marksheet){
                $markSheetStudentID[] = $marksheet->studentID;
               }

               foreach($admitcards as $admitcard){
                $admitCardStudentID[] = $admitcard->studentID;
               }
              foreach($users as $key=>$user){?>
							<tr>
								<td><?=$key+1;?></td>
								<td>
                <?php if(in_array($user->studentID,$markSheetStudentID)){?>
									<a href="<?=base_url('mark-sheet/'.base64_encode($user->studentID))?>"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Download MarkSheet" target="_blank">Download Marksheet <i class="fa fa-download"></i> </i></a>
								<?php }else{?>
									<a href="javascript:void(0);"  class="btn btn-danger btn-sm" data-toggle="tooltip" title="No Marksheet uploaded">No Marksheet uploaded</a>
                  <?php } ?>

                  <?php if(in_array($user->studentID,$admitCardStudentID)){?>
									<a href="<?=base_url('admit-card/'.base64_encode($user->studentID))?>"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Download MarkSheet" target="_blank">Download Admit Card <i class="fa fa-download"></i> </i></a>
								<?php }else{?>
									<a href="javascript:void(0);"  class="btn btn-danger btn-sm" data-toggle="tooltip" title="No Marksheet uploaded">No Admit Card uploaded</a>
                  <?php } ?>
					    <?php if($user->certificate_status==0){?>
									<a href="javascript:void(0);" onclick="generateCertificate(<?=$user->studentID?>)" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Generate Cetificate">Generate Cetificate <i class="fa fa-upload"></i> </a>
								<?php }else{?>
                  <a href="<?=base_url('certificate/'.base64_encode($user->studentID))?>"  class="btn btn-success btn-sm" data-toggle="tooltip" title="Download Cetificate" target="_blank">Download Cetificate <i class="fa fa-download"></i> </a>
                  <?php } ?>
								<a href="<?=base_url('edit-user/'.base64_encode($user->studentID))?>"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit <?=$user->user_type?>"><i class="fa fa-edit"></i></a>
									
								<a href="<?=base_url('view-user/'.base64_encode($user->studentID))?>"  class="btn btn-warning btn-sm" data-toggle="tooltip" title="View <?=$user->user_type?>"><i class="fa fa-eye"></i></a>
								</td>
								<td><img src="<?=base_url($user->profile_pic)?>" width="100" height="100"></td>
                <td><?= $user->registration_no?></td>
								<td><?= $user->roll_no?></td>
								<td><?= $user->student_name?></td>
								<td><?= $user->student_email?></td>
								<td><?= $user->student_contact?></td>
								<td><?= $user->student_address?></td>
                <td><?= date('d-m-Y',strtotime($user->created_at));?></td>
								<td><a  href="javascript:void(0);" onclick="updateUserStatus('<?=$user->studentID?>', '<?=($user->student_status=='1')?'In-active':'Active'?>')"  data-toggle="tooltip" title="Click to Change Status"><?= $user->student_status == 1 ? '<span class="btn  btn-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td>
							
							</tr>
                            <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="bulkUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/bulk_import_student')?>" id="bulkUploadStudent" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
	  <div class="form-group">
            <label for="name" class="col-form-label">Vendor:</label>
			<select class="form-control" name="vendorID" id="vendorID">
               <option value="">Select Vendor</option>
			   <?php foreach($vendors as $vendor){?>
				<option value="<?=$vendor->id?>"><?=$vendor->name?></option>
				<?php } ?>
			</select>
           </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Student Bulk File:</label>
            <input type="file" class="form-control" name="student_file" id="student_file">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="button" onclick="downloadSample()" class="btn btn-success" >Download Sample <i class="fa fa-download"></i></button>
        <button type="submit" name="submit" class="btn btn-primary">Upload <i class="fa fa-upload"></i></button>
      </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="markSheetUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload <?=$page_title?> Mark Sheet</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/bulk_import_student_marksheet')?>" id="bulkUploadStudentMarksheet" method="POST" enctype="multipart/form-data">
      <div class="modal-body">

          <div class="form-group">
            <label for="name" class="col-form-label">Student Marksheet File:</label>
            <input type="file" class="form-control" name="student_mark_sheet_file" id="student_mark_sheet_file">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="button" onclick="downloadMarkSheetSample()" class="btn btn-success" >Download Sample <i class="fa fa-download"></i></button>
        <button type="submit" name="submit" class="btn btn-primary">Upload <i class="fa fa-upload"></i></button>
      </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="admitCardUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload <?=$page_title?> Admit Card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/bulk_import_student_admit_card')?>" id="bulkUploadStudentAdmitCard" method="POST" enctype="multipart/form-data">
      <div class="modal-body">

          <div class="form-group">
            <label for="name" class="col-form-label">Student Admit Card:</label>
            <input type="file" class="form-control" name="student_admit_card_file" id="student_admit_card_file">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="button" onclick="downloadAdmitCardSample()" class="btn btn-success" >Download Sample <i class="fa fa-download"></i></button>
        <button type="submit" name="submit" class="btn btn-primary">Upload <i class="fa fa-upload"></i></button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- View Class Modal Start -->
<div class="modal fade" id="generateCertificateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Documents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="<?=base_url('user/generate_certificate')?>" id="geneaterdCertificateForm" method="post" enctype="multipart/form-data">
	  <div class="modal-body" id="generateCertificateData">
	  </div>
	  <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" >Upload Documents</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit School Class Modal End -->

<script>

$("form#geneaterdCertificateForm").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   $("form#bulkUploadStudent").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   $("form#bulkUploadStudentMarksheet").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   $("form#bulkUploadStudentAdmitCard").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


function updateUserStatus(user_id, status){
     var messageText  = "You want to "+status+" this user?";
     var confirmText =  'Yes, Change it!';
     var message  ="User status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('user/update_status')?>', 
                method: 'POST',
                data: {userid: user_id, user_status: status},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }
   
  function generateCertificate(id){
    $.ajax({
       url: '<?=base_url('Ajax_controller/generateCertificate')?>',
       type: 'POST',
       data: {id},
       success: function (data) {
        $('#generateCertificateModal').modal('show');
         $('#generateCertificateData').html(data);
       }
     });
   
  }


  function uploadDocument(id){
	$.ajax({
       url: '<?=base_url('Ajax_controller/documentContaint')?>',
       type: 'POST',
       data: {id},
       success: function (data) {
        $('#studentDocumentModal').modal('show');
         $('#documentContaint').html(data);
       }
     });
   //$('#studentDocumentModal').modal('show');
   $('#documentContaint').html('');
  }

  function downloadSample(){
    location.href="<?=base_url('user/download_sample')?>";  
  }

  function downloadMarkSheetSample(){
    location.href="<?=base_url('user/download_mark_sheet_sample')?>";  
  }

  function downloadAdmitCardSample(){
    location.href="<?=base_url('user/download_admit_card_sample')?>";  
  }
</script>