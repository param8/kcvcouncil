<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?=$page_title?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body style="color:#fdfdfd; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
<div class="container">
     <div style="width:70%;background:white;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);
    border-radius: 20px;">
   <center>

           <table width="750px" border="0" cellspacing="0" cellpadding="0" style="background:#fdfdfd;">
              <tbody>
			   <tr>                  
                 <td colspan="5"><img src="<?=base_url('public/certificate_image/top-main.jpg')?>" width="750" height="70"></td>   
                </tr>
                <tr>
                  <td align="left"><img src="<?=base_url('public/certificate_image/top-left.jpg')?>" width="61" height="126"></td>
                 <td colspan="3" align="center" style="color:black;font-size:15px;"><img src="<?=base_url($siteinfo->site_logo)?>" width="130" style="margin-bottom: 10px;"><br><?=$siteinfo->site_name?></td>  
                 <td align="right"><img src="<?=base_url('public/certificate_image/top-right.jpg')?>" width="66" height="126"></td>   
                </tr>
                
                <tr>
                  <td align="left"><img src="<?=base_url('public/certificate_image/left.jpg')?>" width="61" height="205"></td>                  
                  <td align="center" colspan="3">
                  <?php 
                    $datetime1 = date_create($user->start_date);
                      
                    $datetime2 = date_create($user->end_date);
                    
                      // Calculates the difference between DateTime objects
                      $interval = date_diff($datetime1, $datetime2);
                    
                      // Printing result in years & months format
                      $duration = $interval->format('%y years %m months %d days');
                      ?>
                    <strong style="color:#2E2E2E; font-size:20px"><?=$user->student_name?></strong><br>
                    <img src="<?=base_url('public/certificate_image/body.jpg')?>" width="428" height="48"><br>
                    <span style="color:#2E2E2E; font-size:18px">Course :-</span>
                    <span style="color:#2E2E2E;font-size:16px"><strong><?=$user->course?></strong></span><br> 
                    <span style="color:#2E2E2E; font-size:18px">Duration :-</span>
                    <span style="color:#2E2E2E;font-size:16px"><strong><?=$duration?></strong></span><br>
                 
                  </td>                  
                  <td align="right"><img src="<?=base_url('public/certificate_image/right.jpg')?>" width="66" height="205"></td>
                </tr> 
                 
                 <tr>
                  <td align="left"><img src="<?=base_url('public/certificate_image/left-b.jpg')?>" width="61" height="115"> </td>                   
                  <td align="center" valign="bottom">
                  <p style="color:#2E2E2E;font-size:13px;text-align:center;margin-bottom:0;font-weight: 500;"><?=date('d-m-Y',strtotime($user->certificate_issue_date))?></p>
                  <img src="signature-bg.jpg" width="174" height="13" style="margin-bottom:20px;">
                  <p style="color:#2E2E2E;font-size:14px;text-align:center;margin-bottom:0;font-weight: 500;position: relative;top: -17px;">Date</p> 
                  </td>
                  <td align="center" width="250"></td>
                  <td align="center" valign="bottom">
                   <p style="text-align:center; margin-bottom:0"><img src="signature.jpg" style="width:135px;"></p>
                   <img src="signature-bg.jpg" width="174" height="13">
                   <p style="color:#2E2E2E;font-size: 14px;text-align: center;margin-bottom: 0;font-weight: 500;">
                   Ravindran A/I Raghunathan
                   <span style="color:#2E2E2E;font-size: 13px;display: block;">Vice President, HR</span></p>
                 </td>                             
                  <td align="right"><img src="<?=base_url('public/certificate_image/right-b.jpg')?>" width="66" height="115"> </td>
                </tr> 
                             
                <tr>
                  <td colspan="5"><img src="<?=base_url('public/certificate_image/bottom.jpg')?>" width="750" height="83"> </td>
                </tr>
              </tbody>
           </table>
           <button onclick="window.print()" style="background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;">Print Certificate</button>
           </center>
    </div>
</div>


</body>
</html>
