<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=$page_title?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  </head>
  <body style="color:#fdfdfd; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
    <div class="container">
    <?php if($user){?>
      <div style="width:70%;background:white;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);
        border-radius: 20px;">
  
        <center>
          <table width="750px" border="0" cellspacing="0" cellpadding="0" style="background:#fdfdfd;">
            <tbody>
              <tr>
                <td colspan="5"><img src="<?=base_url('public/pdf_img/top-main.jpg')?>" width="750" height="70"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/top-left.jpg')?>" width="61" height="248"></td>
                <td colspan="3" align="center" style="color:black;font-size:15px;">
                  <!-- <img src="logo.svg" width="130" style="margin-bottom: 10px;"><br>This is to acknowledge that -->
                  <table style="width:100%">
                    <tr>
                      <td><img src="<?=base_url($siteinfo->site_logo)?>" width="130" style="margin-bottom: 10px;"></td>
                      <td style="padding-left: 30px;"><span style="   line-height: 45px; font-size: 38px;font-weight: bold;/* font-size: 72px; */background: -webkit-linear-gradient(#22859a, #870000);-webkit-background-clip: text;-webkit-text-fill-color: transparent;"><?=$siteinfo->site_name?></span><br>
                        <small style="padding: 8px 1px 10px 0px; font-size: 77%;text-align: center;display: block;width: 75%;margin: auto;font-style: italic;font-weight: bold;color: #22848d;">Registered Company: U80902UP20200PC125616 Registered Under Ministry Of Corporate Affairs Govt of India </small>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" style="font-size: 15px;padding: 6px 0;font-weight: bold;text-align: center;color: #0b5c1f;">A Unit of Ramkumar Vidya Shiksha Sanshthan (RVSS) Society Act-21, 1850</td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center" style="padding: 6px 0;"><span style="    background-color: #162543;color: #fff;font-weight: 500;padding: 3px 9px;display: inline-block;border-radius: 30px;">(50:BRIT QUALIS CERTIFICATION(UK)</span></td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center" style="padding: 6px 0;"><span style="font-size: 15px;font-weight: bold;text-align: center;color: #1f0b8f;">Govt. of India Regd. No.: UP4300019377 Reg. U.P. Govt. No. :KAP/03397/2019-2020</span></td>
                    </tr>
                  </table>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/top-right.jpg')?>" width="66" height="248"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/left.jpg')?>" width="61" height="205"></td>
                <td align="center" colspan="3">
                  <strong style="color:#7f0506; font-size:46px">STATEMENT OF MARKS</strong><br>
                  <img src="<?=base_url('public/pdf_img/body.jpg')?>" width="90%" height="48"><br>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/right.jpg')?>" width="66" height="205"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="188"></td>
                <td align="center" colspan="3" >
                  <table style="width: 100%;">
                    <tr>
                      <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 50%;  color: #000;  font-weight: 600;">Student Name</div>
                          :
                          <div class="uinput" style="    width: 50%;   "><input type="text"  value="<?=$user->student_name?>" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                      </td>
                      <!-- <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 50%;  color: #000;  font-weight: 600;"> Course Duration </div>:
                          <div class="uinput" style="    width: 50%;   "><input type="text" value="<?//=$user->student_name?>" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                        </td> -->
                    </tr>
                    <tr>
                      <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 50%;  color: #000;  font-weight: 600;">Father/Husband Name
                          </div>
                          :
                          <div class="uinput" style="    width: 50%;   "><input type="text"  value="<?=$user->father_name?>" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                      </td>
                      <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 50%;  color: #000;  font-weight: 600;">Mark Sheet No</div>
                          :
                          <div class="uinput" style="    width: 50%;   "><input type="text" value="<?=$user->mark_sheet_no?>" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <!-- <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 50%;  color: #000;  font-weight: 600;">Surname</div>:
                          <div class="uinput" style="    width: 50%;   "><input type="text"  value="Himansha Singh" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                        </td> -->
                      <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 50%;  color: #000;  font-weight: 600;">Date of Birth</div>
                          :
                          <div class="uinput" style="    width: 50%;   "><input type="text" value="<?=date('d-m-Y'.strtotime($user->dob))?>" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 26.1%;  color: #000;  font-weight: 600;">Mother Name </div>
                          :
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$user->mother_name?>" style="font-weight: 800;  border: none;color: #000;"></div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 26.1%;  color: #000;  font-weight: 600;">Course Name</div>
                          :
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$user->course?>" style="font-weight: 600;width: 100%;  border: none;color: #000;"></div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 26.1%;  color: #000;  font-weight: 600;">Institute Name</div>
                          :
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$siteinfo->site_name?>" style="font-weight: 600;  border: none;color: #d80d0d;width: 100%;"></div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="188"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="324"></td>
                <td colspan="3">
                  <table style="width:100%;    color: #000;">
                    <tr >
                      <td style="border: 2px solid;padding: 8px;    width: 55%; text-align: center;font-weight: 700;">SUBJECT</td>
                      <td style="border: 2px solid;padding: 8px; text-align: center;font-weight: 700;white-space: nowrap;">Objective Marks</td>
                      <td style="border: 2px solid;padding: 8px; text-align: center;font-weight: 700;white-space: nowrap;">Practical Marks</td>
                      <td style="border: 2px solid;padding: 8px; text-align: center;font-weight: 700;">Totall</td>
                    </tr>
                    <tr>
                      <td style="border: 2px solid;padding: 4px;font-size: 18px;font-weight: bold; border-top: none; border-bottom: none;"> </td>
                      <td style="border: 2px solid;padding: 4px; text-align: center;font-weight: 600;">Out Of 50</td>
                      <td style="border: 2px solid;padding: 4px; text-align: center;font-weight: 600;">Out Of 50</td>
                      <td style="border: 2px solid;padding: 4px; text-align: center;font-weight: 600;white-space: nowrap;">Out Of 100</td>
                    </tr>
                    <?php 
                      $markSheet = json_decode($user->mark_sheet);
                      //print_r($markSheet);
                      $marks =array();
                      $total_marks = 0;
                      $countSubject = count($markSheet);
                      foreach ($markSheet as $key => $values){
                       // print_r($values);
                        foreach($values as $key1=>$value){
                          $marks[] = $key1;
                          $vals =  explode(',',$value);
                      ?>
                    <tr >
                      <td style="border: 2px solid; font-size: 16px;padding: 6px;   font-weight: 700;  border-top: none; border-bottom: none;"><?=$key1?> </td>
                      <?php 
                        $totalValuev=0;
                        
                        foreach($vals as $val){
                         ?>
                      <td style="border: 2px solid; text-align: center;font-weight: 600;border-bottom: none;border-top: none;"><?=$val?></td>
                      <?php $totalValuev += $val; } ?>
                      <td style="border: 2px solid; text-align: center;font-weight: 600; border-top: none; border-bottom: none;"><?=$totalValuev?></td>
                    </tr>
                    <?php }  $total_marks += $totalValuev; } ?>
                    <tr >
                      <td style="border: 2px solid; font-size: 16px;     padding: 53px 0;  font-weight: 700;  border-top: none;border-bottom: none;   ">  </td>
                      <td style="border: 2px solid; text-align: center;padding: 53px 0;font-weight: 600; border-top: none; border-bottom: none; "></td>
                      <td style="border: 2px solid; text-align: center;padding: 53px 0;font-weight: 600; border-top: none; border-bottom: none;"></td>
                      <td style="border: 2px solid; text-align: center;padding: 53px 0;font-weight: 600; border-top: none; border-bottom: none;"></td>
                    </tr>
                    <tr >
                      <?php 
                        $percentage =($total_marks)/$countSubject; 
                        ?>
                      <td  colspan="2" style="border: 2px solid; font-size: 16px; padding: 6px;   font-weight: 700;     ">Percentage:<?=$percentage?>% Grade: <?=$percentage>94 ? '+A' : ($percentage>84 ? 'A' : ($percentage>69 ? 'B' : 'C')) ?> Total Marks: <?=$total_marks.':'.$countSubject*100?></td>
                      <td style="border: 2px solid; text-align: center;font-weight: 600;  ">Total</td>
                      <td style="border: 2px solid; text-align: center;font-weight: 600;   "><?=$total_marks?></td>
                    </tr>
                  </table>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="324"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="157"></td>
                <td colspan="3" align="center" style="color:black;font-size:15px;">
                  <!-- <img src="logo.svg" width="130" style="margin-bottom: 10px;"><br>This is to acknowledge that -->
                  <table style="     margin-top: 16px;width: 94%;background-color: #197c4d;color: #fff;font-weight: 700;">
                    <tbody>
                      <tr>
                        <td colspan="" style="border: 1px solid;padding: 5px 0  5px 10px;">Performance :</td>
                        <td colspan="" style="border: 1px solid;">Excellent</td>
                        <td colspan="" style="border: 1px solid;">Very Good</td>
                        <td colspan="" style="border: 1px solid;">Good</td>
                        <td colspan="" style="border: 1px solid;">Average</td>
                      </tr>
                      <tr>
                        <td colspan="" style="border: 1px solid;padding: 5px 0  5px 10px;">Mark Range :</td>
                        <td colspan="" style="border: 1px solid;">(95% - 100%)</td>
                        <td colspan="" style="border: 1px solid;">(94% - 85%)</td>
                        <td colspan="" style="border: 1px solid;">(84% - 70%)</td>
                        <td colspan="" style="border: 1px solid;">(69% - 55%)</td>
                      </tr>
                      <tr>
                        <td colspan="" style="border: 1px solid;padding: 5px 0  5px 10px;">Grade :</td>
                        <td colspan="" style="border: 1px solid;">(+A)</td>
                        <td colspan="" style="border: 1px solid;">(A)</td>
                        <td colspan="" style="border: 1px solid;">(B)</td>
                        <td colspan="" style="border: 1px solid;">(C)</td>
                      </tr>
                    </tbody>
                  </table>
                  <table style="width: 100%;">
                    <tr>
                      <td>
                        <p style="color:#162543 ;text-align: center; margin-top: 10px; font-weight: 500;">Online Certificate Verification available on www.thebrainserver.com</p>
                      </td>
                      </td>
                    </tr>
                  </table>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="157"></td>
              </tr>
              <tr>
                <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="100"> </td>
                <td align="center"  width="300" valign="bottom">
                  <div class="image" style="    align-items: end;     border-bottom: 2px solid #dec286;   display: flex;">
                    <div class="im_g" style="    width: 43%;  height: 70px;">
                      <img src="<?=base_url('public/pdf_img/logo/logo_l.png')?>" style="    width: 100%;height: 70px;    border-radius: 100%;" alt="">
                    </div>
                    <div class="text" style="    width: 100%;"><b style="color:#000">ISSUE DATE : 01/02/2023</b> </div>
                  </div>
                </td>
                <td align="center" width="150"></td>
                <td align="center" width="250" valign="bottom">
                  <p style="text-align:center; margin-bottom:0"><img src="<?=base_url('public/pdf_img/signature.jpg')?>" style="width:135px;"></p>
                  <p style="color:#2E2E2E;font-size: 14px;text-align: center;margin-bottom: 0;font-weight: 500;">
                    Ravindran A/I Raghunathan
                    <span style="color:#2E2E2E;font-size: 13px;display: block;">Vice President, HR</span>
                  </p>
                </td>
                <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="100"> </td>
              </tr>
              <tr>
                <td colspan="5"><img src="<?=base_url('public/pdf_img/bottom.jpg')?>" width="750" height="83"> </td>
              </tr>
            </tbody>
          </table>
          <div>
          <button onclick="window.print()" style="background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;">Print Mark Sheet</button>
          </div>
        </center>
      </div>
      <?php }else{ ?>
        <h1 style="text-align: center; color: red;margin-top:30%"> No Mark Sheet Found</h1>
        <?php } ?>
    </div>
  </body>
</html>