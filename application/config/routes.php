<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'authantication';
$route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;
// Admin Routs
$route['login'] = 'authantication';
$route['dashboard'] = 'dashboard';
$route['agents'] = 'user';
$route['students'] = 'user/students';
$route['vendors'] = 'User/vendor';
$route['create-user/(:any)'] = 'User/create_user/$1';
$route['edit-user/(:any)'] = 'User/edit_user/$1';
$route['view-user/(:any)'] = 'User/view_user/$1';
$route['profile'] = 'User/profile';
$route['my-docouments'] = 'User/student_documents';
$route['admit-card/(:any)'] = 'User/admitcard/$1';
$route['mark-sheet/(:any)'] = 'User/marksheet/$1';
$route['certificate/(:any)'] = 'User/certificate/$1';
$route['i-card/(:any)'] = 'User/icard/$1';

$route['courses-list'] = 'courses';
$route['subjects'] = 'subject';
$route['create-subject'] = 'subject/create_subject';
$route['edit-subject/(:any)'] = 'subject/edit_subject/$1';
$route['approved-courses'] = 'courses/approved_courses';
$route['category'] = 'Category';

$route['about'] = 'setting/about';
$route['orders'] = 'order/orders';
$route['order-invoice/(:any)'] = 'order/view_invoice/$1';
$route['enquiry-list'] = 'setting/enquiry';
$route['site-info'] = 'setting/site_setting';




