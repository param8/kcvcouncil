<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
    //$this->not_admin_logged_in();
     $this->load->model('course_model');
     $this->load->model('user_model');
     $this->load->model('Subject_model');

	}

	public function get_city()
    {
       $stateID =  $this->input->post('stateID');
       $cityID = !empty($this->input->post('cityID')) ? $this->input->post('cityID') : '';
       $cities = $this->Common_model->get_state_wise_city($stateID);
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$cityID==$city->id ? 'selected' : ''?>><?=$city->city?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }

     public function checkEmail(){
      if($this->session->userdata('otp_email')){
        $email = $this->session->userdata('otp_email');
        $otp = $this->input->post('otp');
      }else{
        $email = $this->input->post('email');
      }
      
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if($user){  
        if($user->otp==$otp){
          echo json_encode(['status'=>200, 'message'=>'OTP Match Success']);
        }else{
          echo json_encode(['status'=>302, 'message'=>'Invalid OTP!']);
        }
      }else{
        echo json_encode(['status'=>302, 'message'=>'Invalid email id!']);
      }
     }

     public function sendOTP(){
      $email = $this->input->post('email');
      if(empty($email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter email ID!']);
        exit();
      }
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if($user){
        $otp = rand ( 100000 , 999999 );
      // echo $user->id;
          $data = array(
            'otp' => $otp,
        );
         $update_otp = $this->user_model->update_user_status($data,array('id'=>$user->id));
          if($update_otp){
          $email = $user->email;
          //$site_name  =   $this->input->post('site_name');
          $subject  =  "Reset Password" ;
          $html = "Your reset password otp is ".$otp ;
          $sendmail = sendEmail($email,$subject,$html);
          if($sendmail){
            $session = array(
             'otp_email' => $email,
            );
            $this->session->set_userdata($session);
            echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
        }else{
            echo json_encode(['status'=>403, 'message'=>'Mail not send please try again!']);    
        }  
      } else{
        echo json_encode(['status'=>403, 'message'=>'something went wrong']);
      }
     }else{
      echo json_encode(['status'=>403, 'message'=>'Invalid email id!']);
      }
     }

     public function resetPassword(){
      $email = $this->input->post('email');
      $otp = $this->input->post('otp');
      $password = $this->input->post('password');
      $confirmpassword = $this->input->post('confirmpassword');
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if(empty($otp)){
        echo json_encode(['status'=>403, 'message'=>'Please enter OTP!']);
        exit();
      }
      if(empty($password)){
        echo json_encode(['status'=>403, 'message'=>'Please enter Password!']);
        exit();
      }
      if(empty($confirmpassword)){
        echo json_encode(['status'=>403, 'message'=>'Please enter COnfirm Password!']);
        exit();
      }
      if($user->otp!=$otp){
        echo json_encode(['status'=>403, 'message'=>'Please enter valid otp!']);
        exit();
      }
      if($password!=$confirmpassword){
        echo json_encode(['status'=>403, 'message'=>'Confirm Password and password not match!']);
        exit();
      }

      $data =array(
        'password'=>md5($password),
        'otp'=>0
      );
      $update = $this->user_model->update_user_status($data,array('id'=>$user->id));
      if($update){

        echo json_encode(['status'=>200, 'message'=>'Password Change successfully please login!']);
    }else{
        echo json_encode(['status'=>403, 'message'=>'Password Change failed']);    
    }
      
     }

     public function documentContaint(){
      $id = $this->input->post('id');
      $user=$this->user_model->get_user(array('users.id'=>$id));
      ?>
      
        <div class="form-group">
          <label for="name" class="col-form-label">Admit Card:</label>
          <a href="<?=base_url('admit-card/'.base64_encode($id))?>" class="form-control" name="admit_card" id="admit_card">Admit Card <i class="fa fa-print"></i></a>
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">Mark Sheet:</label>
          <a href="<?=base_url('mark-sheet/'.base64_encode($id))?>" class="form-control" name="admit_card" id="admit_card">Mark Sheet <i class="fa fa-print"></i></a>
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">Certificate:</label>
          <a href="<?=base_url('certificate/'.base64_encode($id))?>" class="form-control" name="admit_card" id="admit_card">Certificate <i class="fa fa-print"></i></a>
        </div>

        <input type="hidden" name="id" value="<?=$id?>" id="id">
      <?php
     }

     public function generateCertificate(){
      $id = $this->input->post('id');
      $user=$this->user_model->get_user(array('users.id'=>$id));
      ?>
      
        <div class="form-group">
          <label for="name" class="col-form-label">Certificate Issue Date:</label>
          <input type="date" name="certificate_issue_date" id="certificate_issue_date" class="form-control">
        </div>

        <input type="hidden" name="id" value="<?=$id?>" id="id">
      <?php
     }
}