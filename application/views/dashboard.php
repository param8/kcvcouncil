
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		 <!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
					<div class="box bg-primary-light">
						<div class="box-body d-flex px-0">
							<div class="flex-grow-1 p-30 flex-grow-1 bg-img dask-bg bg-none-md" style="background-position: right bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-1.svg)">
								<div class="row ">
									<div class="col-12 col-xl-7">
										<h2>Welcome back, <strong><?php echo $this->session->userdata('name'); ?>!</strong></h2>
									</div>
									<div class="col-12 col-xl-5"></div>
								</div>
							</div>
						</div>
					</div>
					<?php if($this->session->userdata('user_type')=='Admin'){?>
					<div class="row">
					<div class="col-xl-4">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
                               <h4 class="font-weight-400">Total Students</h4>
								<p class="font-size-20">
								<?=count($users);?>
								</p>
								</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4">
							<div class="box">
								<div class="box-body d-flex p-0">
									<div class="flex-grow-1 bg-danger p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
										<h4 class="font-weight-400">Total Agents</h4>
										<p class="font-size-20">
										<?=count($agents);?>
										</p>
									</div>
								</div>
							</div>
						</div>
				
						<div class="col-xl-4">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-success p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
									<h4 class="font-weight-400">Total Vendor</h4>
									<p class="font-size-20">
									<?=count($vendors);?>
								    </p>
									</div>
								</div>
							</div>
                         </div>
				
					</div>
					<?php } elseif($this->session->userdata('user_type')=='Agent'){?>
						<div class="row">
				
						<div class="col-xl-6">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
                               <h4 class="font-weight-400">Total Students</h4>
								<p class="font-size-20">
								<?=count($users);?>
								</p>
								</div>
								</div>
							</div>
						</div>
						
				
					</div>
					<?php } elseif($this->session->userdata('user_type')=='Vendor'){?>
						<div class="row">
				
						<div class="col-xl-6">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
                               <h4 class="font-weight-400">Total Students</h4>
								<p class="font-size-20">
								<?=count($users);?>
								</p>
								</div>
								</div>
							</div>
					</div>
				
					</div>
					<?php }?>
				
	
	

