<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-user"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="">
        <form action="<?=base_url('user/update')?>" id="addusers" method="POST" enctype="multipart/form-data">
          <div class="row">

          <?php if($user_type == 'Student'){?>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"><?=$user_type?> Registration No.:</label>
                <input type="text" class="form-control"  value="<?=$student->registration_no?>" disabled>
              </div>
            </div>

           <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Vendor:</label>
                <select class="form-control" name="vendorID" id="vendorID" disabled>
                  <option value="">Select vendor</option>
                  <?php foreach($vendors as $vendor){?>
                  <option value="<?=$vendor->id?>" <?=$user->vendorID==$vendor->id ? 'selected' : ''?>><?=$vendor->name?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            
         <?php } ?>
   
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"><?=$user_type?> Name:</label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>" disabled>
              </div>
            </div>
        
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Email:</label>
                <input type="text" class="form-control"  value="<?=$user->email?>" disabled>
              </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Contact No:</label>
                <input type="text" class="form-control" disabled name="contact" id="contact" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->contact?>">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Address:</label>
                <textarea  class="form-control" name="address" disabled id="address"><?=$user->address?></textarea>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">State:</label>
                <select class="form-control" name="state" id="state" disabled onchange="getCity(this.value)">
                  <option value="">Select State</option>
                  <?php foreach($states as $state){?>
                  <option value="<?=$state->id?>" <?=$user->state==$state->id ? 'selected' : ''?>><?=$state->name?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">City:</label>
                <select class="form-control" name="city" id="city" disabled>
                </select>
              </div>
            </div>

            <?php if($user_type == 'Student'){?>
              <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Father Name:</label>
                <input type="text"  class="form-control" disabled  name="father_name" id="father_name" value="<?=$student->father_name?>">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Mother Name:</label>
                <input type="text"  class="form-control" disabled  name="mother_name" id="mother_name" value="<?=$student->mother_name?>">
              </div>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Roll No.:</label>
                <input type="text"  class="form-control" disabled  name="roll_no" id="roll_no" value="<?=$student->roll_no?>">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Date of Birth.:</label>
                <input type="text" onfocus="(this.type='date')" disabled class="form-control"   name="dob" id="dob" value="<?=date('d-m-Y',strtotime($student->dob))?>">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course.:</label>
                <input type="text"  class="form-control" disabled  name="course" id="course" value="<?=$student->course?>">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course Start Date.:</label>
                <input type="text"  class="form-control" disabled  name="course" id="course" value="<?=$student->start_date?>">
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course End Date.:</label>
                <input type="text"  class="form-control" disabled  name="course" id="course" value="<?=$student->end_date?>">
              </div>
            </div>
            <?php 
            $datetime1 = date_create($student->start_date);
              
            $datetime2 = date_create($student->end_date);
             
              // Calculates the difference between DateTime objects
              $interval = date_diff($datetime1, $datetime2);
            
              // Printing result in years & months format
              $duration = $interval->format('%y years %m months %d days');
              ?>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course Duration.:</label>
                <input type="text"  class="form-control" disabled  name="course" id="course" value="<?=$duration?>">
              </div>
            </div>
            

            <?php   } ?>

           
            
          <div class="modal-footer">
            <a href="<?=$user_type == 'Student' ? base_url('students') : ($user_type == 'Vendor' ? base_url('vendors') : base_url('agents')) ?>" name="submit" class="btn btn-primary">Back</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
  <!-- /.box -->          
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">

  $( document ).ready(function() {
      getCity(<?=$user->state?>,<?=$user->city?>);
     });
  
</script>