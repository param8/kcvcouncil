
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title=='Agent' ? '<i class="fa fa-user-secret"></i>' : '<i class="fa fa-user"></i>'?> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
		  <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="<?=base_url('create-user/'.base64_encode($page_title))?>" class="btn btn-primary btn-sm float-right" >Add <?=$page_title?> <i class="fa fa-plus"></i></a>
			  
            </div>
          </div>
				</div>
			  <div class="box">
				<div class="box-header with-border">
			
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
								<th>Action</th>
								<th>Image</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Address</th>
								<th>Created Date</th>
								<th>Status</th>
						
							</tr>
						</thead>
						<tbody>
             <?php foreach($users as $key=>$user){?>
							<tr>
								<td><?=$key+1;?></td>
								<td>
								<?php if($user->user_type == 'Student'){?>
									<a href="javascript:void(0);" onclick="uploadDocument(<?=$user->id?>)" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Upload Documents"><i class="fa fa-upload"></i> <i class="fa fa-file"></i></a>
									<?php } ?>
								<a href="<?=base_url('edit-user/'.base64_encode($user->id))?>"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit <?=$user->user_type?>"><i class="fa fa-edit"></i></a>
									
								<a href="<?=base_url('view-user/'.base64_encode($user->id))?>"  class="btn btn-warning btn-sm" data-toggle="tooltip" title="View <?=$user->user_type?>"><i class="fa fa-eye"></i></a>
								</td>
								<td><img src="<?=base_url($user->profile_pic)?>" width="100" height="100"></td>
								<td><?= $user->name?></td>
								<td><?= $user->email?></td>
								<td><?= $user->contact?></td>
								<td><?= $user->address?></td>
                <td><?= date('d-m-Y',strtotime($user->created_at));?></td>
								<td><a  href="javascript:void(0);" onclick="updateUserStatus('<?=$user->id?>', '<?=($user->status=='1')?'In-active':'Active'?>')"  data-toggle="tooltip" title="Click to Change Status"><?= $user->status == 1 ? '<span class="btn  btn-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td>
							
							</tr>
             <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="bulkUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/bulk_import_student')?>" id="bulkUploadStudent" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
	  <div class="form-group">
            <label for="name" class="col-form-label">Vendor:</label>
			<select class="form-control" name="vendorID" id="vendorID">
               <option value="">Select Vendor</option>
			   <?php foreach($vendors as $vendor){?>
				<option value="<?=$vendor->id?>"><?=$vendor->name?></option>
				<?php } ?>
			</select>
           </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Student Bulk File:</label>
            <input type="file" class="form-control" name="student_file" id="student_file">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="button" onclick="downloadSample()" class="btn btn-success" >Download Sample <i class="fa fa-download"></i></button>
        <button type="submit" name="submit" class="btn btn-primary">Upload <i class="fa fa-upload"></i></button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- View Class Modal Start -->
<div class="modal fade" id="studentDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Documents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="<?=base_url('user/upload_documents')?>" id="uploadDocumentForm" method="post" enctype="multipart/form-data">
	  <div class="modal-body" id="documentContaint">
	  </div>
	  <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" >Upload Documents</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit School Class Modal End -->

<script>

$("form#uploadDocumentForm").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   $("form#bulkUploadStudent").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


function updateUserStatus(user_id, status){
  //alert(status);
     var messageText  = "You want to "+status+" this user?";
     var confirmText =  'Yes, Change it!';
     var message  ="User status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('user/update_status')?>', 
                method: 'POST',
                data: {userid: user_id, user_status: status},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }



  function uploadDocument(id){
	$.ajax({
       url: '<?=base_url('Ajax_controller/documentContaint')?>',
       type: 'POST',
       data: {id},
       success: function (data) {
        $('#studentDocumentModal').modal('show');
         $('#documentContaint').html(data);
       }
     });
   //$('#studentDocumentModal').modal('show');
   $('#documentContaint').html('');
  }

  function downloadSample(){
    location.href="<?=base_url('user/download_sample')?>";  
  }
</script>