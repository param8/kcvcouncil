<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
 
<style>  
  @media screen and (max-width: 580px) {
   
   body{
    /* font-size: 18px; */
   } 
   .bg_body{
    padding: 28% 4% 10% 4%!important;
    width: 100%!important;
   }
   .issu_date {
    margin-top: 17%;
}
.s_name{
  font-size: 15px!important;
}
.mw {
    width: 60%!important;
}

}
  </style>
</head>

<body style="color:#333!important; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
<div class="container">
     <div class="bg_body" style="width:50%;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);background-image: url(<?=base_url('public/icard/icard.png')?>);
    background-size: 100% 100%;  padding: 11% 4% 2% 4%;-webkit-print-color-adjust: exact !important;">
   <center>
           <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" ">
              <tbody>
                <tr>
                  <td>
                    <table style="width: 100%;">
                      <tbody>
                     <tr>
                      <td align="center" colspan="4"><p style="color: black;text-align: center;padding-top: 22px;
                        font-weight: 600;
                        font-size: 18px;"><img src="<?=base_url($user->profile_pic)?>" alt="" style="width: 27%;
    box-shadow: 0px 0px 8px 1px #626060;"></td>
                     </tr>
                     <tr>
                      <td align="center" colspan="4"><h2 style="color: black;text-align: center;font-size: 31px;
                        font-weight: 700;    margin-bottom: 31px;">Enrollment No..</h2></td>
                     </tr>
                     <tr>
                      <td align="" colspan="4">
                        <div class="stu_detail" style=" display: flex; justify-content: space-around;justify-content: flex-start;   gap: 10px;
                        width: 100%;   margin: auto;     margin-bottom: 20px;">
                          <div class="s_name" style="color: black;    font-weight: bold;    font-size: 18px;">Student Name : </div>
                          <div class="s_name mw" style="width: 68%;"><input type="text" placeholder=" " value="<?=$user->student_name?>" readonly style="border: navajowhite;
                            border-bottom: 2px dashed #000;    outline: none;width: 100%;    background-color: transparent;"> </div>
                        </div>
                      </td>
                     </tr>
                     <tr>
                      <td align="" colspan="4">
                        <div class="stu_detail" style=" display: flex; justify-content: space-around;justify-content: flex-start;   gap: 10px;
                        width: 100%;   margin: auto;     margin-bottom: 20px;">
                          <div class="s_name" style="color: black;    font-weight: bold;    font-size: 18px;">Father's Name : </div>
                          <div class="s_name mw" style="width: 68%;"><input type="text" placeholder=" " value="<?=$user->father_name?>" readonly style="border: navajowhite;
                            border-bottom: 2px dashed #000;    outline: none;width: 100%;    background-color: transparent;"> </div>
                        </div>
                      </td>
                     </tr>
                     <tr>
                      <td align="" colspan="4">
                        <div class="stu_detail" style=" display: flex; justify-content: space-around;justify-content: flex-start;   gap: 10px;
                        width: 100%;   margin: auto;     margin-bottom: 20px;">
                          <div class="s_name" style="color: black;    font-weight: bold;    font-size: 18px;">Joining Date : </div>
                          <div class="s_name mw" style="width: 71%;"><input type="text" placeholder=" " value="<?=date('d-m-Y',strtotime($user->created_at))?>" readonly style="border: navajowhite;
                            border-bottom: 2px dashed #000;    outline: none;width: 100%;    background-color: transparent;"> </div>
                        </div>
                      </td>
                     </tr>
                     <tr>
                      <td align="" colspan="4">
                        <div class="stu_detail" style=" display: flex; justify-content: space-around;justify-content: flex-start;   gap: 10px;
                        width: 100%;   margin: auto;     margin-bottom: 20px;">
                          <div class="s_name" style="color: black;    font-weight: bold;    font-size: 18px;">Course  : </div>
                          <div class="s_name mw" style="width: 81%;"><input type="text" placeholder=" " value="<?=$user->course?>" readonly style="border: navajowhite;
                            border-bottom: 2px dashed #000;    outline: none;width: 100%;    background-color: transparent;"> </div>
                        </div>
                      </td>
                     </tr>
                     <tr>
                      <td align="" colspan="4">
                        <div class="stu_detail" style=" display: flex; justify-content: space-around;justify-content: flex-start;   gap: 10px;
                        width: 100%;   margin: auto;     margin-bottom: 20px;">
                          <div class="s_name" style="color: black;    font-weight: bold;    font-size: 18px;">Years : </div>
                          <div class="s_name mw" style="width: 80%;"><input type="text" placeholder=" " style="border: navajowhite;
                            border-bottom: 2px dashed #000;    outline: none;width: 100%;    background-color: transparent;"> </div>
                        </div>
                      </td>
                     </tr>
                     <tr>
                      <td align="" colspan="4">
                        <div class="stu_detail" style=" display: flex; justify-content: space-around;justify-content: flex-start;   gap: 10px;
                        width: 100%;   margin: auto;     margin-bottom: 20px;">
                          <div class="s_name" style="color: black;    font-weight: bold;    font-size: 18px;">Branch : </div>
                          <div class="s_name mw" style="width: 80%;"><input type="text" placeholder=" " style="border: navajowhite;
                            border-bottom: 2px dashed #000;    outline: none;width: 100%;    background-color: transparent;"> </div>
                        </div>
                      </td>
                     </tr>
                     <tr>
                      <td align="center" colspan="4"><p style="color: black;text-align: center; padding: 1% 0; font-size: 20px; font-weight: 500;">Contact: 8318016165, 7275959459, 808181141</p></td>
                     </tr>
                      <tr>
                      <td align="center" colspan="4">
                        <div class="email_infi" style="display: flex;justify-content: space-between;">
                          <span style="    font-size: 16px;
                          font-weight: 500;"><b>E-mail:</b> info@kcvcouncil.com</span> <span style="    font-size: 16px;
                          font-weight: 500;">  <b>Website:</b> www.kcvcouncil.com</span>
                        </div>
                      </td>
                     </tr>
                     
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
           </table>
           </center>
    </div>
</div>


</body>
</html>
