<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Certificate</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lobster|Great+Vibes|Satisfy|Inconsolata|Open+Sans'
      rel='stylesheet' />
    <style>
      @media screen and (max-width: 580px) {
      body {}
      .bg_body {
      padding: 44% 4% 34% 4% !important;
      width: 100% !important;
      }
      .issu_date {
      margin-top: 17%;
      }
      .ct_in {
      width: 70% !important;
      font-size: 12px !important;
      }
      .ct {
      font-size: 14px !important;
      }
      .en {
      font-size: 13px !important;
      }
      .grad_d {
      font-size: 22px !important;
      }
      .mon {
      width: 40% !important;
      font-size: 14px !important;
      }
      .gr {
      width: 17% !important;
      font-size: 14px !important;
      }
      .add_r {
      width: 60% !important;
      font-size: 14px !important;
      }
      p {
      margin: 0 !important;
      padding: 2px !important;
      }
      }
    </style>
  </head>
  <body style="color:#000!important; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
    <div class="container">
      <div class="bg_body" style="width:70%;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);background-image: url(<?=base_url('public/marksheets/crtifaction.jpg')?>);
        background-size: 100% 100%;  padding:11% 4% 11% 4%; -webkit-print-color-adjust: exact;">
        <center>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" ">
            <tbody>
              <tr>
                <td align="left" class="ct" style="color: black; font-size: 16px;  font-style: italic;"><b>Certificate No
                  :</b> <input class="ct_in" type="" value="KCV000<?=$user->id?>" readonly
                    style="border: navajowhite;
                    border-bottom: 2px dashed #000;    outline: none;width: 34%;    background-color: transparent;font-weight: 500;">
                </td>
                <td align="right" style="color: black; font-size: 16px;  font-style: italic;" class="en"><b>Enrollment No
                  :</b> <input class="en_in" type="" readonly value="<?=$user->registration_no?>"
                    style="border: navajowhite;
                    border-bottom: 2px dashed #000;    outline: none;width: 45%;    background-color: transparent;font-weight: 500;">
                </td>
              </tr>
              <tr>
                <td align="center" colspan="2">
                  <p style="color: #100f0f;text-align: center;padding-top: 17%;
                    font-size: 32px;font-family:  Great Vibes; ">This credential is awarded to</p>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <table style="width: 100%;">
                    <tr>
                      <td align="right"><span style="    font-weight: bold; ;color: #000;    font-family:math;">Mr /
                        Mrs</span>
                      </td>
                      <td><input type="text" value="<?=$user->student_name?>" readonly placeholder=" "
                        style="border: none;
                        border-bottom: 2px dashed #000;  outline: none;    color: #000;
                        margin-left: 7px;  text-align: center;  background-color: transparent; margin-bottom: 24px;padding: 4px 0;  font-family: sans-serif;"></td>
                    </tr>
                    <tr>
                      <td align="right"><span style="    font-weight: bold; ;color: #000;    font-family: math;">Son /
                        Daughter of</span>
                      </td>
                      <td><input type="text" value="<?=$user->father_name?>" readonly placeholder=" "
                        style="border: none;
                        border-bottom: 2px dashed #000;  outline: none;    color: #000;
                        margin-left: 7px;   text-align: center;  background-color: transparent; margin-bottom: 24px; padding: 4px 0; font-family: sans-serif;"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="2">
                  <p style="color: #100f0f;text-align: center;padding-top: 22px;
                    font-size: 32px;font-family:  Great Vibes;    margin-bottom: 3%; ">for successfully completing
                    the course
                  </p>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="2"><input type="text"
                  value=" <?=$user->course?>" readonly placeholder=" "
                  style="border: none;
                  border-bottom: 1px dashed #000;
                  outline: none; width: 80%;text-align: center;    margin-bottom: 3%; background-color: transparent; font-family: sans-serif;   color: #000; font-weight: bold;"></td>
              </tr>
              <tr>
                <td align="center" colspan="2">
                  <p class="grad_d" style="color: #100f0f;text-align: center; 
                    font-size: 32px;font-family:  Great Vibes;    padding: 14px 31px; ">from our
                    <input type="" class="add_r" value="<?=$user->student_address?>" style="  border: none;
                      border-bottom: 1px solid #000;
                      outline: none;
                      width: 47%;
                      text-align: center;
                      background-color: transparent;
                      font-family: sans-serif;
                      color: #000;
                      font-weight: bold;
                      font-size: 21px;"> center with <input type="text " class="gr" value="A+" style="   border: none;
                      border-bottom: 1px solid #000;
                      outline: none;
                      width: 10%;
                      text-align: center;
                      background-color: transparent;
                      font-family: sans-serif;
                      color: #000;
                      font-weight: bold;
                      font-size: 21px;">
                    grade and his/her course duration was
                    Centre Director <input type="text" value="" readonly class="mon" style=" border: none;
                      border-bottom: 1px solid #000;
                      outline: none;
                      width: 11%;
                      text-align: center;
                      background-color: transparent;
                      font-family: sans-serif;
                      color: #000;
                      font-weight: bold;
                      font-size: 17px;
                      ">
                  </p>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <table style="width: 100%;     margin-top: 3%;">
                    <tr>
                      <td style="width: 20%;" align="center">
                        <input type="text"
                          style="    width: 95%;
                          border: none;  border-bottom: 1px solid #000;
                          outline: none;  text-align: center; background-color: transparent; font-family: sans-serif;  color: #000; font-weight: bold;font-size: 21px;">
                        <span style="    font-size: 12px;
                          font-weight: 500;"> Centre Director</span>
                      </td>
                      <td align="center">
                        <p style="color: #100f0f;
                          text-align: center;
                          padding-top: -4px;
                          font-size: 14px;
                          font-family: sans-serif;
                          font-weight: bold; "><span>Grade: A: 75%, A: 60% 75%, B: 50% < 60%, C: < 50% </span>
                        </p>
                      </td>
                      <td style="width: 20%;" align="center">
                        <img src="<?=base_url('public/marksheets/sign.PNG')?>" alt="" style="    width: 56%;
                          display: block;">
                        <span style="    font-size: 12px;
                          font-weight: 500;">Chief Executive Officer</span>
                      </td>
                    </tr>
                    <!-- <tr>
                      <td style="width: 40%;">
                        <div class="issu_date" style="display: flex;    gap: 10px;">
                          <img src="./logo.PNG" alt="" style="    width: 30%;">
                          <span>
                          <input type="text"
                            style="    width: 95%;
                            border: none;  border-bottom: 1px solid #000;
                            outline: none;  text-align: center; background-color: transparent; font-family: sans-serif;  color: #000; font-weight: bold;font-size: 21px;">
                          <span style="    font-size: 12px;
                            font-weight: 500;"> Centre Director</span>
                          </span>
                        </div>
                      </td>
                    </tr> -->
                  </table>
                </td>
              </tr>
              <!-- end -->
            </tbody>
          </table>
        </center>
      </div>
    </div>
  </body>
</html>