<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Mark Sheets</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link href='https://fonts.googleapis.com/css?family=Lobster|Great+Vibes|Satisfy|Inconsolata|Open+Sans'
    rel='stylesheet' />
  <meta content=" " viewport>
  <style>
  @media screen and (max-width: 580px) {
    .bg_body {
      padding: 66% 4% 35% 4% !important;
      width: 100% !important;
    }

    .container {
      width: 100% !important;
    }

    .c_in {
      width: 80% !important;
    }

    .add_d {
      width: 33% !important;
    }

    .add_img {
      width: 22% !important;
    }

    .s_image img {
      width: 69% !important;
    }

    .m_de {
      font-size: 22px;
    }

    .c_hef {
      width: 40% !important;
    }
  }
  </style>
</head>

<body
  style="color:#000!important;font-size: 16px; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
  <div class="container">
    <div class="bg_body" style="width:70%;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);background-image: url(<?=base_url('public/marksheets/marksheet.jpg')?>);
        background-size: 100% 100%;  padding:18% 4% 7% 4%;-webkit-print-color-adjust: exact !important;">
      <center>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" ">
          <tbody>
            <tr>
              <td align="left" style="color: black; font-size: 16px;  font-style: italic;"><b>Certificate No :</b>
                <input type="" class="c_in" value="KCV000<?=$user->id?>"
                  style="border: navajowhite;
                    border-bottom: 2px dashed #000;    outline: none;width: 34%;    background-color: transparent;font-weight: 500;" readonly>
              </td>
              <td align="right" style="color: black; font-size: 16px;  font-style: italic;"><b>Enrollment No :</b>
                <input type="" class="c_in" value="<?=$user->registration_no?>"
                  style="border: navajowhite;
                    border-bottom: 2px dashed #000;    outline: none;width: 45%;    background-color: transparent;font-weight: 500;" readonly>
              </td>
            </tr>
            <tr>
              <td align="center" colspan="2">
                <p class="m_de" style="color: #100f0f;text-align: center;padding-top: 4%;
                    font-size: 32px;  font-weight: bold; ">MarkSheet Detail </p>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <table style="width: 100%;">
                  <tr>
                    <td style=" width: 70%; " class="add_d">
                      <div class="all_detail">
                        <div class="s_detail">
                          <div style="margin-bottom: 10px;"> <b>Student Name :</b> <?=$user->student_name?></div>
                          <div style="margin-bottom: 10px;"> <b> Father's Name :</b> <?=$user->father_name?></div>
                          <div style="margin-bottom: 10px;"> <b>Course : </b> <?=$user->course?>
                          </div>
                        </div>
                      </div>
                    <td align="right" style=" width: 30%; " class="add_img">
                      <div class="s_image">
                        <!-- <span style="    font-weight: 700; ">Duration 09 Months</span> -->
                        <img src="<?=base_url($user->profile_pic)?>" alt="" style="    width:35%;">
                      </div>
                    </td>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <div class="s_detail" style="display: flex;justify-content: space-between; margin-top: 15px;">
                  <div style="margin-bottom: 10px;">
                    <b>Examination Centre</b> <?=$user->exam_center?>
                  </div>
                  <div class="s_image">
                    <b> Examination Held</b> on <?=date('d-m-Y',strtotime($user->exam_date))?>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <table class="" style="    margin-top: 5%;width: 100%; border: 2px solid #787878;text-align: center;">
                  <thead>
                    <tr>
                      <th style="border: 2px solid #787878;padding: 3px; background-color: #212925;color: #fff;">S.No
                      </th>
                      <th style="border: 2px solid #787878;padding: 3px; background-color: #212925;color: #fff;">Subject
                      </th>
                      <th style="border: 2px solid #787878;padding: 3px; background-color: #212925;color: #fff;">Total
                        Marks
                      </th>
                      <th style="border: 2px solid #787878;padding: 3px; background-color: #212925;color: #fff;">Passing
                        Marks
                      </th>
                      <th style="border: 2px solid #787878;padding: 3px; background-color: #212925;color: #fff;">Marks
                        Obtained
                      </th>
                    </tr>
                  </thead>

                  <?php 
                      $markSheet = json_decode($user->mark_sheet);
                    $sno = 1;
                    $total_marks = 0;
                    $passing_marks = 0;
                    $obtain_marks = 0;
                      foreach ($markSheet as $key => $values){
                       $total_marks += $values[0];
                       $passing_marks += $values[1];
                       $obtain_marks += $values[2];
                       $percentage = ($obtain_marks*100)/$total_marks;
                      ?>
                  <tr>
                    <td
                      style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600;border-top: none;border-bottom: none;">
                      <?=$sno?>
                    </td>
                    <td
                      style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600;border-top: none;border-bottom: none; text-align: left;">
                      <?=$key?>
                    </td>
                    <?php foreach($values as $key_marks=>$value){
                        
                      ?>
                    <td
                      style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600;border-top: none;border-bottom: none;">
                      <?=$value?>
                    </td>
                    <?php } ?>

                  </tr>
                  <?php $sno = $sno+1; } ?>

                  <tr>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> </td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; ;text-align: right;">
                      <b style="color: red" ;>Total</b>
                    </td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> <?=$total_marks?></td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> <?=$passing_marks?></td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> <?=$obtain_marks?></td>
                  </tr>
                  <tr>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> </td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; ;text-align: right;">
                      <b style="color: red" ;>Percentage</b>
                    </td>
                    <td colspan="2"
                      style="border: 2px solid #787878;  background-color: #dfdcdc;   padding: 10px 2px;font-weight: 600; ">
                    </td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> <?=number_format($percentage, 2, '.', '') . '%'?></td>
                  </tr>
                  <!-- <tr>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> </td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; ;text-align: right;">
                      <b style="color: red" ;>Percentage</b>
                    </td>
                    <td colspan="2"
                      style="border: 2px solid #787878;  background-color: #dfdcdc;   padding: 10px 2px;font-weight: 600; ">
                    </td>
                    <td style="border: 2px solid #787878;    padding: 10px 2px;font-weight: 600; "> A+</td>
                  </tr> -->
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" colspan="2">
                <div style="display: flex;
                          align-items: center;">
                  <div class="sign" style="    width: 40%;    padding-top: 9px;">
                    <input type="text"
                      style="    width: 50%;
                              border: none;  border-bottom: 1px solid #000;
                              outline: none;  text-align: center; background-color: transparent; font-family: sans-serif;  color: #000; font-weight: bold;font-size: 21px;">
                    <span style="    font-size: 14px;
                              font-weight: 500;display: block;"> Centre Director</span>
                    <div style="margin-top: 3%;">
                      <input type="text" value="<?=date('d-m-Y',strtotime($user->created_at))?>" style="width: 50%;border: none;  border-bottom: 1px solid #000;    display: block;outline: none;  text-align: center; background-color: transparent; font-family: sans-serif;  color: #000; font-weight: bold;font-size: 21px;">
                      <span style="    font-size: 14px;
                                font-weight: 700;"> Date</span>
                    </div>
                  </div>
                  <div class="c_hef">
                    <img src="<?=base_url('public/marksheets/sign.PNG')?>" alt="" style="    width: 50%;
                              display: block;    margin-top: 9%;">
                    <span style="    font-size: 14px;
                              font-weight: 500;">Chief Executive Officer</span>
                  </div>
                </div>
              </td>
            </tr>
        </table>
        </td>
        </tr>
        <tr>
          <td colspan="2">
            <p style="    width: 95%; margin-top:1%;text-align:  center;"><a href="" style="color: red;">
                www.kcvcouncil.com</a>
            </p>
          </td>
        </tr>
        <!-- end -->
        </tbody>
        </table>
      </center>
    </div>
  </div>
</body>

</html>