<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('user_model');
		$this->load->model('Auth_model');
		$this->load->model('setting_model');
    $this->load->library('csvimport');
    $this->load->helper('download');
	}

	public function index()
	{	$data['page_title'] = 'Agent';
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Agent'));
		$this->admin_template('users',$data);
	}

	public function students()
	{
 
    $data['page_title'] = 'Student';
    $studentCondition = $this->session->userdata('user_type')=='Vendor' ? array('users.vendorID'=>$this->session->userdata('id') ) : array('users.user_type' => 'Student');
    $data['users'] = $this->user_model->get_students($studentCondition);
    $condition = $this->session->userdata('user_type')=='Vendor' ? array('users.id'=>$this->session->userdata('id') ) : array('users.user_type' => 'Vendor');
    $data['vendors'] = $this->user_model->get_all_users($condition);
    $data['marksheets'] = $this->user_model->download_mark_sheets(array('users.status'=>1));
    $data['admitcards'] = $this->user_model->download_admit_cards(array('users.status'=>1));
		$this->admin_template('students',$data);
	}

	public function vendor()
	{	$data['page_title'] = 'Vendor';
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Vendor'));
		$this->admin_template('users',$data);
	}

	public function create_user()
	{   
		$data['user_type'] = base64_decode($this->uri->segment(2));
		$data['page_title'] = 'Create '.$data['user_type'];
		$condition = $this->session->userdata('user_type') == 'Vendor' ? array('users.user_type' => 'Vendor','users.id' => $this->session->userdata('id')) :array('users.user_type' => 'Vendor');
    $data['vendors'] = $this->user_model->get_all_users($condition);
		$this->admin_template('create-user',$data);
	}

	public function store(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type = $this->input->post('user_type');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$vendorID =  $user_type == 'Student' ? $this->input->post('vendorID') : '';
    $password = $contact;
 

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' name']); 	
			exit();
		}

    if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' email address']); 	
			exit();
		}

		if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' mobile']); 	
			exit();
		}
	
		$checkEmail = $this->user_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}

		$checkMobile = $this->user_model->get_user(array('contact'=>$contact));
		if($checkMobile){
			echo json_encode(['status'=>403,'message'=>'This mobile is already in use']);
			exit();
		}
   
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  address']); 	
			exit();
		}

		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please slecte state']); 	
			exit();
		}

		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please slecte city']); 	
			exit();
		}

		if($user_type == 'Student'){
			$father_name = $this->input->post('father_name');
      $mother_name = $this->input->post('mother_name');
      $roll_no= $this->input->post('roll_no');
      $dob = $this->input->post('dob');
      $course = $this->input->post('course');
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');

      if(empty($father_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' father name']); 	
        exit();
      }
  
      if(empty($mother_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' mother name']); 	
        exit();
      }
  


      if(empty($roll_no)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' roll no.']); 	
        exit();
      }

      $checkrollNO = $this->user_model->get_student(array('students.roll_no'=>$roll_no));
      if($checkrollNO){
        echo json_encode(['status'=>403,'message'=>'This Roll no. is already in use']);
        exit();
      }

      if(empty($dob)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' date of birth.']); 	
        exit();
      }

  

      if(empty($course)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' course.']); 	
        exit();
      }

      if(empty($start_date)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' course start date.']); 	
        exit();
      }

      if(empty($end_date)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' course end date.']); 	
        exit();
      }

		}
    $this->load->library('upload');
    if($_FILES['profile_pic']['name']!= '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
     }elseif(!empty($user->profile_pic)){
        $image = $user->profile_pic;
     }
    else{
        $image = 'public/dummy_user.jpg';
    }
    
	  $ran_id = rand(time(), 100000000);
		$data = array(
			'unique_id'   => $ran_id,
			'name'        => $name,
			'email'       => $email,
			'password'    => md5($password),
			'contact'     =>$contact,
			'vendorID'    => $vendorID,
			'address'     => $address,
			'user_type'   => $user_type,
      'profile_pic' => $image,
      'state'       => $state,
      'city'        => $city,
			'login_status' =>'Offline now',
			'otp' =>0,
		);
		$register = $this->Auth_model->register($data);

		if($register){
	  if($user_type=='Student'){
      $student = $this->user_model->get_student(array());
      $reg = $student->id+1;
      $regidterID = 'NKCV0000'.$reg;
        $dataStudent = array(
          'studentID'       => $register,
          'registration_no' => $regidterID,
          'roll_no'         => $roll_no,
          'father_name'     => $father_name,
          'mother_name'     => $mother_name,
          'dob'             => $dob,
          'course'          => $course,
          'start_date'      => $start_date,
          'end_date'        => $end_date,
        );
        $this->user_model->store_student_detail($dataStudent);
      }
		  echo json_encode(['status'=>200, 'message'=>$user_type.' added successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	}


  public function edit_user()
	{   
    $id = base64_decode($this->uri->segment(2));
    $data['user']=$this->user_model->get_user(array('users.id'=>$id));
    //print_r($data['user']);die;
    $data['user_id']=$data['user']->id;
		$data['user_type'] = $data['user']->user_type;
		$data['page_title'] = 'Edit '.$data['user_type'];
    $data['vendors'] = $this->user_model->get_all_users(array('users.user_type' => 'Vendor'));
    if($data['user_type'] == 'Student'){
      $data['student'] = $this->user_model->get_student(array('studentID'=>$id));
      //print_r($data['student']);die;
    }
		$this->admin_template('edit-user',$data);
	}

	public function update(){
    $u_id = $this->input->post('user_id');
		$name = $this->input->post('name');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type = $this->input->post('user_type');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$vendorID =  $user_type == 'Student' ? $this->input->post('vendorID') : '';
    $password = $contact;
    $user = $this->user_model->get_user(array('users.id'=>$u_id));

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' name']); 	
			exit();
		}

		if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' mobile']); 	
			exit();
		}

		$checkMobile = $this->user_model->get_user(array('users.contact'=>$contact,'users.id<>'=>$u_id));
		if($checkMobile){
			echo json_encode(['status'=>403,'message'=>'This mobile is already in use']);
			exit();
		}
   
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  address']); 	
			exit();
		}

		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please slecte state']); 	
			exit();
		}

		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please slecte city']); 	
			exit();
		}

		if($user_type == 'Student'){
			$father_name = $this->input->post('father_name');
      $mother_name = $this->input->post('mother_name');
      $roll_no= $this->input->post('roll_no');
      $dob = $this->input->post('dob');
      $course = $this->input->post('course');
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');

      if(empty($father_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' father name']); 	
        exit();
      }
  
      if(empty($mother_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' mother name']); 	
        exit();
      }

      if(empty($roll_no)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' roll no.']); 	
        exit();
      }

      $checkrollNO = $this->user_model->get_student(array('students.roll_no'=>$roll_no,'students.studentID <>'=>$u_id));
      if($checkrollNO){
        echo json_encode(['status'=>403,'message'=>'This Roll no. is already in use']);
        exit();
      }

      if(empty($dob)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' date of birth.']); 	
        exit();
      }

      if(empty($course)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' course.']); 	
        exit();
      }

      if(empty($start_date)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' course start date.']); 	
        exit();
      }

      if(empty($end_date)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' course end date.']); 	
        exit();
      }

		}

    $this->load->library('upload');
    if($_FILES['profile_pic']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','','profilePIC'.$name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $profile_pic = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($user->profile_pic)){
      $profile_pic = $user->profile_pic;
    }else{
      $profile_pic = '';
    }

		$data = array(
			'name'        => $name,
			'contact'     => $contact,
			'address'     => $address,
			'user_type'   => $user_type,
      'vendorID'    => $vendorID,
      'state'       =>$state,
      'city'        =>$city,
      'profile_pic' =>$profile_pic,
		);
    //print_r($data);die;
		$update = $this->user_model->update_user($data,array('id'=>$u_id));
	  if($user_type=='Student'){
      $studentData = array(
        'roll_no'      => $roll_no,
        'father_name'  => $father_name,
        'mother_name'  => $mother_name,
        'dob'          => $dob,
        'course'       => $course,
        'start_date'      => $start_date,
        'end_date'        => $end_date,
      );
      $this->user_model->update_student($studentData,array('studentID'=>$u_id));
    }
		if($update){

		  echo json_encode(['status'=>200, 'message'=>$user_type.' Updated successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	}


  public function view_user()
	{   
    $id = base64_decode($this->uri->segment(2));
    $data['user']=$this->user_model->get_user(array('users.id'=>$id));
    //print_r($data['user']);die;
    $data['user_id']=$data['user']->id;
		$data['user_type'] = $data['user']->user_type;
		$data['page_title'] = 'View '.$data['user_type'];
    $data['vendors'] = $this->user_model->get_all_users(array('users.user_type' => 'Vendor'));
    if($data['user_type'] == 'Student'){
      $data['student'] = $this->user_model->get_student(array('studentID'=>$id));
      //print_r($data['student']);die;
    }
		$this->admin_template('view-user',$data);
	}

	public function update_status()
	{
		$user_id = $this->input->post('userid');
		$user_status = ($this->input->post('user_status')=='Active')?'1':'0';

        $userdata =  array(
          'status' => $user_status
        );
	
        $update_status =  $this->user_model->update_user_status($userdata,array('id'=>$user_id));
     
	}


  public function upload_documents(){
    $id = $this->input->post('id');
    $student=$this->user_model->get_student(array('users.id'=>$id));
    $this->load->library('upload');
    if($_FILES['admit_card']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/student_documents',
      'file_name' 	=> str_replace(' ','','admitCard'.$student->student_name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('admit_card'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['admit_card']['name']);
        $type = $type[count($type) - 1];
        $admit_card = 'uploads/student_documents/'.$config['file_name'].'.'.$type;
        $history_admit_card = 'uploads/student_documents/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($student->admit_card)){
      $admit_card = $student->admit_card;
      $history_admit_card = "";
    }else{
      $admit_card = '';
      $history_admit_card = "";
    }


    if($_FILES['mark_sheet']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/student_documents',
      'file_name' 	=> str_replace(' ','','markSheet'.$student->student_name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('mark_sheet'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['mark_sheet']['name']);
        $type = $type[count($type) - 1];
        $mark_sheet = 'uploads/student_documents/'.$config['file_name'].'.'.$type;
        $history_mark_sheet = 'uploads/student_documents/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($student->mark_sheet)){
      $mark_sheet = $student->mark_sheet;
      $history_mark_sheet = "";
    }else{
      $mark_sheet = '';
      $history_mark_sheet = "";
    }
  
    if($_FILES['certificate']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/student_documents',
      'file_name' 	=> str_replace(' ','','certificate'.$student->student_name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('certificate'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['certificate']['name']);
        $type = $type[count($type) - 1];
        $certificate = 'uploads/student_documents/'.$config['file_name'].'.'.$type;
        $history_certificate = 'uploads/student_documents/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($student->certificate)){
      $certificate = $student->certificate;
      $history_certificate = "";
    }else{
      $certificate = '';
      $history_certificate = "";
    }

    $data = array(
      'admit_card' => $admit_card,
      'mark_sheet' => $mark_sheet,
      'certificate' => $certificate
    );

    $update = $this->user_model->update_student($data,array('studentID'=>$id));

		if($update){

      $studentDocument = array(
        'studentID'   => $id,
        'admit_card'  => $history_admit_card,
        'mark_sheet'  => $history_mark_sheet,
        'certificate' => $history_certificate
      );
      $update = $this->user_model->store_document_history($studentDocument);
		  echo json_encode(['status'=>200, 'message'=>$student->student_name.' documents uploaded successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}

  }

public function download_sample(){
  $file_name = 'sample/student_sample.csv';
  $file = file_get_contents(base_url($file_name));
  force_download($file_name, $file);
}

public function download_mark_sheet_sample(){
  $file_name = 'sample/student_maksheet_sample.csv';
  $file = file_get_contents(base_url($file_name));
  force_download($file_name, $file);
}

public function download_admit_card_sample(){
  $file_name = 'sample/student_admitcard_sample.csv';
  $file = file_get_contents(base_url($file_name));
  force_download($file_name, $file);
}
  
public function bulk_import_student(){
  $vendorID = !empty($this->input->post('vendorID')) ? $this->input->post('vendorID') : 0;
  if (isset($_FILES["student_file"])) {
    $config['upload_path']   = "uploads/csv/";
    $config['allowed_types'] = 'text/plain|text/csv|csv';
    $config['max_size']      = '2048';
    $config['file_name']     = $_FILES["student_file"]['name'];
    $config['overwrite']     = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("student_file")) {
      echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
    } else {
      $file_data = $this->upload->data();
      $file_path = 'uploads/csv/' . $file_data['file_name'];
      if ($this->csvimport->get_array($file_path)) {
        $csv_array = $this->csvimport->get_array($file_path);
        foreach ($csv_array as $key => $row) {
        
          $checkEmail = $this->user_model->get_user(array('email'=>$row['email']));
          if($checkEmail){
            continue;
          }

          $checkMobile = $this->user_model->get_user(array('contact'=>$row['contact']));
          if($checkMobile){
            continue;
          }
          $ran_id = rand(time(), 100000000);
            $insert_data = array(
            'unique_id' => $ran_id,
            'vendorID' => $vendorID,
            'name' => $row['name'],
            'email' =>  $row['email'],
            'contact' => $row['contact'],
            'password' => md5($row['contact']),
            'address' => $row['address'],
            'user_type' => 'Student',
            'profile_pic' => 'public/dummy_user.jpg',
            "login_status" => 'Offline now',
          );
          $register = $this->Auth_model->register($insert_data);
        if($register){
          $student = $this->user_model->get_student(array());
          $reg = $student->id+1;
          $regidterID = 'NKCV0000'.$reg;
          $dataStudent = array(
              'studentID'       => $register,
              'registration_no' => $regidterID,
              'admission_no'    => $row['admission_no'],
              'roll_no'         => $row['roll_no'],
              'father_name'     => $row['father_name'],
              'mother_name'     => $row['mother_name'],
              'dob'             => date('d-m-Y',strtotime($row['dob'])),
              'course'          => $row['course'],
              'start_date'          => $row['start_date'],
              'end_date'          => $row['end_date'],
            );
            $this->user_model->store_student_detail($dataStudent);
          }
          
        }
        echo json_encode(['status'=>200, 'message'=>'Student Uploaded Successfully']);
      } else {
        echo json_encode(['status'=>403, 'message'=>'Student Uploaded Failure']);
      }
    }
  } else {
    echo json_encode(['status'=>403, 'message'=>'something went wrong']);
  }
}



public function bulk_import_student_marksheet(){

  if (isset($_FILES["student_mark_sheet_file"])) {
    $config['upload_path']   = "uploads/csv/";
    $config['allowed_types'] = 'text/plain|text/csv|csv';
    $config['max_size']      = '2048';
    $config['file_name']     = $_FILES["student_mark_sheet_file"]['name'];
    $config['overwrite']     = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("student_mark_sheet_file")) {
      echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
    } else {
      $file_data = $this->upload->data();
      $file_path = 'uploads/csv/' . $file_data['file_name'];
      if ($this->csvimport->get_array($file_path)) {
        $csv_array = $this->csvimport->get_array($file_path);
        foreach ($csv_array as $key => $row) {
         // print_r($row);
          $student = $this->user_model->get_student(array('students.registration_no'=>$row['registration_no']));
          if($student){
            $mark_sheet_no = bin2hex(random_bytes(16));
             $studentID = $student->studentID;
             $counter = (count($row)-2)/4;
             $marks_with_subject = array();
             for($i=1;$i<=$counter;$i++){
                 $subject = $row['subject'.$i];
                 $total_marks = $row['total_marks'.$i];
                 $passing_marks = $row['passing_marks'.$i];
                 $obtain_marks = $row['obtain_marks'.$i];
                 $marks_with_subject[$subject] = array($total_marks, $passing_marks,$obtain_marks); 
                 
             }
             $data =array(
              'mark_sheet_no' => $mark_sheet_no,
              'studentID' => $studentID,
              'registration_no' => $row['registration_no'],
              'mark_sheet' => json_encode($marks_with_subject),
              'exam_center' => $row['exam_center'],
              'exam_date' => $row['exam_date']
             );

             $store = $this->user_model->store_mark_sheet($data);
          }

          
          
        }
        echo json_encode(['status'=>200, 'message'=>'Mark Sheet Uploaded Successfully']);
      } else {
        echo json_encode(['status'=>403, 'message'=>'Mark Sheet Uploaded Failure']);
      }
    }
  } else {
    echo json_encode(['status'=>403, 'message'=>'something went wrong']);
  }
}


public function bulk_import_student_admit_card(){
  if (isset($_FILES["student_admit_card_file"])) {
    $config['upload_path']   = "uploads/csv/";
    $config['allowed_types'] = 'text/plain|text/csv|csv';
    $config['max_size']      = '2048';
    $config['file_name']     = $_FILES["student_admit_card_file"]['name'];
    $config['overwrite']     = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("student_admit_card_file")) {
      echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
    } else {
      $file_data = $this->upload->data();
      $file_path = 'uploads/csv/' . $file_data['file_name'];
      if ($this->csvimport->get_array($file_path)) {
        $csv_array = $this->csvimport->get_array($file_path);
        foreach ($csv_array as $key => $row) {
         // print_r($row);
          $student = $this->user_model->get_student(array('students.registration_no'=>$row['registration_no']));
          if($student){
            $studentID = $student->studentID;
             $data =array(
              'studentID' => $studentID,
              'registration_no' => $row['registration_no'],
              'center' => $row['center'],
              'exam_date' => $row['exam_date'],
             );

             $store = $this->user_model->store_admit_card($data);
          }
          
        }
        echo json_encode(['status'=>200, 'message'=>'Mark Sheet Uploaded Successfully']);
      } else {
        echo json_encode(['status'=>403, 'message'=>'Mark Sheet Uploaded Failure']);
      }
    }
  } else {
    echo json_encode(['status'=>403, 'message'=>'something went wrong']);
  }
}

	public function download_document(){
    $file_name = base64_decode($this->uri->segment(3));
    $file = file_get_contents(base_url($file_name));
    force_download($file_name, $file);
  }

  public function marksheet(){
    $data['siteinfo'] = $this->siteinfo();
    $id = base64_decode($this->uri->segment(2));
    $data['user'] = $this->user_model->download_mark_sheet(array('student_mark_sheet.studentID' => $id));
    $data['page_title'] = 'Mark Sheet';
   // print_r($data);die;
    $this->load->library('pdf');
    $html = $this->load->view('mark_sheet', $data);
   // $this->pdf->createPDF($html, 'mypdf', false);
  }

  public function certificate(){
    $data['siteinfo'] = $this->siteinfo();
    $id = base64_decode($this->uri->segment(2));
    $data['user'] = $this->user_model->get_student(array('students.studentID' => $id));
    $data['page_title'] = 'Certificate';
   // print_r($data);die;
    $this->load->library('pdf');
    $html = $this->load->view('certificate', $data);
   // $this->pdf->createPDF($html, 'mypdf', false);
  }

  public function icard(){
    $data['siteinfo'] = $this->siteinfo();
    $id = base64_decode($this->uri->segment(2));
    $data['user'] = $this->user_model->get_student(array('students.studentID' => $id));
    $data['page_title'] = 'Certificate';
   // print_r($data);die;
    $this->load->library('pdf');
    $html = $this->load->view('icard', $data);
   // $this->pdf->createPDF($html, 'mypdf', false);
  }

  public function profile(){
    $id = $this->session->userdata('id');
    $data['user']=$this->user_model->get_user(array('users.id'=>$id));
    $data['user_id']=$data['user']->id;
		$data['user_type'] = $data['user']->user_type;
		$data['page_title'] = 'Profile';
    $this->admin_template('profile', $data);
  }

    public function update_profile(){
      $u_id = $this->input->post('user_id');
      $name = $this->input->post('name');
      $contact = $this->input->post('contact');
      $address= $this->input->post('address');
      $user_type = $this->input->post('user_type');
      $state = $this->input->post('state');
      $city = $this->input->post('city');
      $user = $this->user_model->get_user(array('users.id'=>$u_id));
  
      if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' name']); 	
        exit();
      }
  
      if(empty($contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' mobile']); 	
        exit();
      }
  
      $checkMobile = $this->user_model->get_user(array('users.contact'=>$contact,'users.id<>'=>$u_id));
      if($checkMobile){
        echo json_encode(['status'=>403,'message'=>'This mobile is already in use']);
        exit();
      }
     
      if(empty($address)){
        echo json_encode(['status'=>403, 'message'=>'Please enter  address']); 	
        exit();
      }
  
      if(empty($state)){
        echo json_encode(['status'=>403, 'message'=>'Please slecte state']); 	
        exit();
      }
  
      if(empty($city)){
        echo json_encode(['status'=>403, 'message'=>'Please slecte city']); 	
        exit();
      }
  

  
  
      $this->load->library('upload');
      if($_FILES['profile_pic']['name'] != '')
      {
      $config = array(
        'upload_path' 	=> 'uploads/users',
        'file_name' 	=> str_replace(' ','','profilePIC'.$name).uniqid(),
        'allowed_types' => '*',
        'max_size' 		=> '10000000',
      );
          $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('profile_pic'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]);
            exit();
        }
        else
        {
          $type = explode('.',$_FILES['profile_pic']['name']);
          $type = $type[count($type) - 1];
          $profile_pic = 'uploads/users/'.$config['file_name'].'.'.$type;
        }
      }elseif(!empty($user->profile_pic)){
        $profile_pic = $user->profile_pic;
      }else{
        $profile_pic = '';
      }
  
      $data = array(
        'name'        => $name,
        'contact'     => $contact,
        'address'     => $address,
        'user_type'   => $user_type,
        'state'       =>$state,
        'city'        =>$city,
        'profile_pic' =>$profile_pic,
      );
      //print_r($data);die;
      $update = $this->user_model->update_user($data,array('id'=>$u_id));

      if($update){
  
        echo json_encode(['status'=>200, 'message'=>$user_type.' Updated successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
      }
    }

    public function student_documents(){
      $id = $this->session->userdata('id');
      $data['marksheets'] = $this->user_model->download_mark_sheet(array('student_mark_sheet.studentID'=>$id));
      $data['admitcards'] = $this->user_model->download_admit_card(array('student_admit_card.studentID'=>$id));
      $data['user']=$this->user_model->get_student(array('students.studentID'=>$id));
      $data['user_id']=$id;
      $data['user_type'] = $data['user']->user_type;
      $data['page_title'] = 'MY Documents';
      $this->admin_template('my-docouments', $data);
    }

    public function generate_certificate(){
      $id = $this->input->post('id');
      $certificate_issue_date = $this->input->post('certificate_issue_date');
      if(empty($certificate_issue_date)){
        echo json_encode(['status'=>302, 'message'=>'Please provide certificate issue date!']);
        exit();
      }
      $studentData =array(
        'certificate_status' =>1,
        'certificate_issue_date' => $certificate_issue_date,
       );

      $update =  $this->user_model->update_student($studentData,array('studentID'=>$id));
      if($update){
        echo json_encode(['status'=>200, 'message'=>'Certificate Generated successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
      }
    }


  }