<?php 
class Subject extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('course_model');
		$this->load->model('Subject_model');
        $this->load->model('category_model');
        $this->load->model('teachers_model');
	}




  public function index()
  {	
	  $data['page_title'] = 'Subject';
	  $condition =  array('subjects.status'=>1) ;
	  $data['subjects'] = $this->Subject_model->get_subjects($condition);
	  $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
	  $this->admin_template('subject',$data);
  }

  public function store_subject(){

		$courseID = $this->input->post('courseID');
		$subject = $this->input->post('subject');
		//$check = $this->course_model->get_course(array('courses.course'=>$course,'courses.userID'=>$userID));
		// if($check){
		//   echo json_encode(['status'=>403,'message'=>'This course already exists']);
		//   exit();
		// }
	
		if(empty($courseID)){
		  echo json_encode(['status'=>403,'message'=>'Please select a course']);
		  exit();
		}
	
		if(empty($subject)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a subject']);
		  exit();
		}
        $array_filetr = array_filter($_FILES['subject_pdf_upload']['name']);
		if(!empty($array_filetr)){
			 $pdf_value = array();
			 $total_course_pdf = count($_FILES['subject_pdf_upload']['name'])-1;
			for($i = 0; $i < $total_course_pdf;$i++){
			  $uid = uniqid();
			  $filename = $_FILES['subject_pdf_upload']['name'][$i];
			  $f_maxsize = 41943040;
			  $f_ext_allowed = array("pdf");
			  $f_name_2 = str_replace(" ","_", htmlspecialchars($filename));
			  $f_size  =  $filename;
			  $f_tmp   =  $_FILES["subject_pdf_upload"]['tmp_name'][$i];
			  $f_error =  $_FILES["subject_pdf_upload"]['error'][$i];
			  $f_ext = pathinfo($f_name_2, PATHINFO_EXTENSION); 
			  $files = $uid.$filename;
			   move_uploaded_file($f_tmp, "uploads/courses_pdf/".$files);   
			   $pdf_value[] = array(
               $this->input->post('subject_pdf_title')[$i] => $files
			   );             
			}
			
			$subject_pdf_upload=json_encode($pdf_value);
		}else{
			$subject_pdf_upload ="";
		}
	
	   $data = array(
		'courseID' => $courseID,
		'subject' => $subject,
		'subject_pdf_upload' => $subject_pdf_upload,
	   );
	
	   $store = $this->Subject_model->store_subject($data);
	   if($store){
		 echo json_encode(['status'=>200, 'message'=>'Subject add successfully!']);
		 }else{
		   echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	
  }

  public function edit_subject_form(){
	$subjectID = $this->input->post('subjectID');
	$subject = $this->Subject_model->get_subject(array('subjects.id' => $subjectID));
	$courses = $this->course_model->get_courses(array('courses.status'=>1));
	?>
	<div class="form-group">
        <label for="name" class="col-form-label">Course:</label>
        <select class="form-control" name="courseID" id="courseID">
          <option value="">Select Course</option>
          <?php foreach($courses as $course){?>
              <option value="<?=$course->id?>" <?=$subject->courseID ==$course->id ? 'selected' : ''  ?>><?=$course->course?></option>
          <?php } ?>
        </select>
        </div>
        <div class="form-group">
          <label for="name" class="col-form-label">Subject Name:</label>
          <input type="text" class="form-control" name="subject" id="subject" value="<?=$subject->subject?>">
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label>Upload Course PDF</label>   
				
                <div class="input-group control-group after-add-more">
                  <div class="row">
				    <?php if($subject->subject_pdf_upload == ""){?> 
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="subject_pdf_title[]"  class="form-control" placeholder="Enter PDF Title">  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="subject_pdf_upload[]"  class="form-control" placeholder="Enter Name Here">  
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 input-group-btn">   
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>   
                  </div>
				  <?php } else{ ?>
				<div class="col-lg-2 col-md-2 input-group-btn">   
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>   
                  </div>
				<?php
			} ?>
                </div>
			
				
              </div>
              <div class="form-group copy hide">
                <div class="control-group input-group" style="margin-top:10px">
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="subject_pdf_title[]"  class="form-control" placeholder="Enter PDF Title">  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="subject_pdf_upload[]"  class="form-control" placeholder="Enter Name Here">  
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 input-group-btn">   
                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>  
                  </div>
                </div>
              </div>
            </div>
        
	<?php
  }
	
}