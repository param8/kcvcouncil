<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}



	public function get_all_users($condition)
	{
		$this->db->select('users.*,site_info.discription,site_info.facebook_url,site_info.insta_url,site_info.twitter_url,site_info.linkedin_url,site_info.youtube_url,states.name as stateName,cities.city as cityName');
		$this->db->from('users');
		$this->db->join('site_info','site_info.adminID = users.id','left');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($condition);
	    $this->db->order_by('users.id','desc');
		return $this->db->get()->result();
	}



	public function get_user($condition){
		$this->db->select('users.*,states.name as stateName,cities.city as cityName');
		$this->db->from('users');
		$this->db->join('site_info','site_info.adminID = users.id','left');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($condition);
		$query =  $this->db->get()->row();
		return $query;
	}

	public function get_student($condition){
    $this->db->select('students.*,users.name as student_name,users.email as student_email,users.contact as student_contact,users.address as student_address,users.status as student_status ,users.user_type ,users.profile_pic');
    $this->db->from('students');
    $this->db->join('users', 'users.id = students.studentID','left');
    $this->db->where($condition);
    $this->db->order_by('students.id','desc');
	  return $this->db->get()->row();
	}

  public function get_students($condition){
    $this->db->select('students.*,users.name as student_name,users.email as student_email,users.contact as student_contact,users.address as student_address,users.status as student_status ,users.user_type ,users.profile_pic,u.name as vendor_name');
    $this->db->from('students');
    $this->db->join('users', 'users.id = students.studentID','left');
	$this->db->join('users u', 'u.id=users.vendorID', 'left');
    $this->db->where($condition);
    $this->db->order_by('students.id','desc');
	  return $this->db->get()->result();
  }

	public function update_student($studentData,$condition){
		$this->db->where($condition);
	 return $this->db->update('students',$studentData);
	}

	public function update_user($data,$condition)
	{
		$this->db->where($condition);
		return $this->db->update('users',$data);
	}

	public function update_user_status($data,$id)
	{
		$this->db->where($id);
		return $this->db->update('users',$data);
     //echo $this->db->last_query();die;
	}

	public function get_user_details($data){
		$this->db->select('users.*,states.name as stateName,cities.city as cityName');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($data);
		$result =  $this->db->get('users');
			return $result->row();	
	}
	
	public function stor_enquiry($data){
		return $this->db->insert('contact_us', $data);
	   }

	   public function store_student_detail($data){
		return $this->db->insert('students', $data);
	   }

	   public function update_student_detail($data,$id){
		$this->db->where('studentID',$id);
		return $this->db->update('students',$data);
	   }

     public function store_document_history($studentDocument){
      return $this->db->insert('student_document_history',$studentDocument);
     }

     public function store_mark_sheet($data){
      return $this->db->insert('student_mark_sheet',$data);
     }

     public function store_admit_card($data){
      return $this->db->insert('student_admit_card',$data);
     }

	 public function download_mark_sheet($condition){
		$this->db->select('student_mark_sheet.*,users.name as student_name,users.email as student_email,users.contact as student_contact,users.address as student_address,users.status as student_status ,
		users.user_type ,users.profile_pic,students.roll_no,students.father_name,students.mother_name,students.dob,students.course,u.name as vendor_name');
        $this->db->join('users', 'users.id=student_mark_sheet.studentID', 'left');
		$this->db->join('students', 'students.studentID=student_mark_sheet.studentID', 'left');
		$this->db->join('users u', 'u.id=users.vendorID', 'left');
		$this->db->where($condition);
	    return $this->db->get('student_mark_sheet')->row();
	 }

	 public function download_mark_sheets($condition){
		$this->db->select('student_mark_sheet.*,users.name as student_name,users.email as student_email,users.contact as student_contact,users.address as student_address,users.status as student_status ,
		users.user_type ,users.profile_pic,students.roll_no,students.father_name,students.mother_name,students.dob,students.course,u.name as vendor_name');
        $this->db->join('users', 'users.id=student_mark_sheet.studentID', 'left');
		$this->db->join('students', 'students.studentID=student_mark_sheet.studentID', 'left');
		$this->db->join('users u', 'u.id=users.vendorID', 'left');
		$this->db->where($condition);
	    return $this->db->get('student_mark_sheet')->result();
	 }

	 public function download_admit_cards($condition){
		$this->db->select('student_admit_card.*,users.name as student_name,users.email as student_email,users.contact as student_contact,users.address as student_address,users.status as student_status ,
		users.user_type ,users.profile_pic,students.roll_no,students.father_name,students.mother_name,students.dob,students.course,u.name as vendor_name');
        $this->db->join('users', 'users.id=student_admit_card.studentID', 'left');
		$this->db->join('students', 'students.studentID=student_admit_card.studentID', 'left');
		$this->db->join('users u', 'u.id=users.vendorID', 'left');
		$this->db->where($condition);
	    return $this->db->get('student_admit_card')->result();
	 }

	 public function download_admit_card($condition){
		$this->db->select('student_admit_card.*,users.name as student_name,users.email as student_email,users.contact as student_contact,users.address as student_address,users.status as student_status ,
		users.user_type ,users.profile_pic,students.roll_no,students.father_name,students.mother_name,students.dob,students.course,u.name as vendor_name');
        $this->db->join('users', 'users.id=student_admit_card.studentID', 'left');
		$this->db->join('students', 'students.studentID=student_admit_card.studentID', 'left');
		$this->db->join('users u', 'u.id=users.vendorID', 'left');
		$this->db->where($condition);
	    return $this->db->get('student_admit_card')->row();
	 }
}