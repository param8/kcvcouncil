<?php 
class Teachers_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_teachers($condition){
    $this->db->select('teachers.*,subjects.subject,subjects.id as subject_ID');
    $this->db->from('teachers');
    $this->db->join('subjects', 'subjects.id = teachers.subjectID','left');
    $this->db->where($condition);
    return $this->db->get()->result();
}

public function get_teacher($condition){
  $this->db->select('teachers.*,subjects.subject,subjects.id as subject_ID');
  $this->db->from('teachers');
  $this->db->join('subjects', 'subjects.id = teachers.subjectID','left');
  $this->db->where($condition);
  return $this->db->get()->row();
}

//   public function get_course($condition){
//     $this->db->select('courses.*,subjects.subject as subject_name,subjects.id as subjectID');
//     $this->db->from('courses');
//     $this->db->join('subjects', 'subjects.id = courses.subject','left');
//     $this->db->where($condition);
//     return $this->db->get()->row();
//   }

//   public function get_more_courses(){
//     $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id')) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city')):'') ;
//     $this->db->select('courses.*,subjects.subject as subject_name,subjects.id as subjectID');
//     $this->db->from('courses');
//     $this->db->join('subjects', 'subjects.id = courses.subject','left');
//     $this->db->where($condition);
//     $this->db->order_by('rand()');
//     $this->db->limit(3);
//     return $this->db->get()->result();
// }

  public function store_teacher($data){
    return $this->db->insert('teachers',$data);
  }




}